import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdSnackBar } from '@angular/material';

import { UserInfoService } from '../services/UserInfo/user-info.service';
import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';
import { phoneValidator, emailValidator, userExistsValidatorFactory } from '../validators/pt-validators';

import { Observable } from 'rxjs';

@Component({
  selector: 'pt-pm-account',
  templateUrl: './pt-pm-account.component.html',
  styleUrls: ['./pt-pm-account.component.scss']
})
export class PtPmAccountComponent implements OnInit {
  info_form: FormGroup;
  constructor(private userInfo: UserInfoService, private pm: ProjectManagerService,
              private fb: FormBuilder, private snackBar: MdSnackBar) {}

  onSave() {
    var save = this.info_form.value;
    console.log(save);
    this.snackBar.open('Updating info...');
    this.pm.updatePmInfo(save).then( () => {
      this.snackBar.open('Updated info!', '', { duration: 1500 });
    })
    .catch( () => {
      this.snackBar.open('An Error Occured!', '', { duration: 1500 });
    })
  }

  ngOnInit() {
    Observable.from(this.userInfo.auth)
      .first()
      .mergeMap( info => Observable.from(this.pm.getUserByUid(info.uid)).first() )
      .subscribe( info => {
        this.info_form = this.fb.group({
          name: [info.name, Validators.required],
          email: [info.email, Validators.compose([Validators.required, emailValidator]), userExistsValidatorFactory(this.pm, [info.email])],
          phone: [info.phone, Validators.compose([Validators.required, phoneValidator])]
        });

        this.info_form.updateValueAndValidity();
      });
  }

}
