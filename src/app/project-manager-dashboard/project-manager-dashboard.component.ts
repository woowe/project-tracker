import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductLogosService } from '../services/ProductLogos/product-logos.service';
import { UserInfoService } from '../services/UserInfo/user-info.service';
import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';
import { AngularFire, AuthProviders, AuthMethods, FirebaseObjectObservable } from 'angularfire2';
import { MdDialog, MdDialogRef } from '@angular/material';

import { Observable, BehaviorSubject } from 'rxjs';

import { formatDate, getProductType, productNameEq, objectEq } from '../utils/utils';

@Component({
  selector: 'app-project-manager-dashboard',
  templateUrl: './project-manager-dashboard.component.html',
  styleUrls: ['./project-manager-dashboard.component.scss']
})
export class ProjectManagerDashboardComponent implements OnInit {
  constructor(private userInfo: UserInfoService) {}

  ngOnInit() {
  }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: './add-dealership-dialog.component.html',
  styleUrls: ['./add-dealership-dialog.component.css'],
})
export class AddDealershipDialog implements OnInit {
  products: any = [];
  milestone_templates: any[] = [];

  filtered_users: any[] = [];
  added_users: any[] = [];
  add_user: any = {};
  removed_users: any[] = [];
  c_users: any = {};

  dealership_uid: string = null;

  dealership_model: any = {};
  dealership_form_group: FormGroup;

  constructor(public dialogRef: MdDialogRef<AddDealershipDialog>, private projectManager: ProjectManagerService,
              private fb: FormBuilder) {
    
  }

  onDealershipInfoChange(event: any) {
    if(typeof event === 'object' && event.name && event.location) {

      console.log("INFO CHANGE", event);
      this.dealership_model = Object.assign(this.dealership_model, {name: event.name, location: event.location});
    }
  }

  ngOnInit() {
    var dealership = this.dialogRef.config.data.dealership;
    if(!dealership) {
      dealership = {};
    }
    // location = address, locality, short_admin_area_l1 zip, short_country
    console.log('DEALERSHIP', dealership);
    var location = dealership.location;
    if(!dealership.location && dealership.address && dealership.administrative_area_l1 && dealership.locality && dealership.postal_code) {
      location = `${dealership.address}, ${dealership.locality}, ${dealership.administrative_area_l1} ${dealership.postal_code}`;
      if(dealership.country) {
        location += `, ${dealership.country}`;
      }

      dealership.location = location;
    }
    this.dealership_form_group = this.fb.group({
      'name': [dealership.name, Validators.required],
      'location': [location, Validators.required],
    });
    
    this.projectManager.getAllProductTemplates().subscribe(templates => {
      if(dealership.products) {
        for(let d_product of dealership.products) {
          let product_idx = templates.findIndex(p => productNameEq(p.name, d_product.name));
          // console.log("D PRODUCT", d_product, templates[product_idx], product_idx)
          if(product_idx >= 0 && product_idx !== null ) {
            d_product.selected = true;
            d_product.op = "UPDATE";
            templates[product_idx] = d_product;
          }
        }
      }

      this.products = templates;
      console.log('Product templates: ', this.products);
    });
    
    
    this.projectManager.getAllMilestoneTemplates().subscribe(templates => {
      this.milestone_templates = templates;
      console.log('Milestone templates: ', this.milestone_templates);
    });

    
    this.projectManager.getAllCustomers().subscribe(users => {
      for(let user of users) {
        this.c_users[user.$key] = "UPDATE";
      }
      this.filtered_users = users;
    });

    if(dealership.users) {
      if(dealership.users.length > 0) {
        this.projectManager.getDealershipUsers(dealership.$key).subscribe( (users: any) => {
          for(let user of users) {
            this.c_users[user.$key] = "ADDED";
            let user_exsists_idx = this.added_users.findIndex(au => au.$key === user.$key);
            if(user_exsists_idx >= 0 && user_exsists_idx !== null) {
              this.added_users[user_exsists_idx] = user;
            } else {
              this.added_users = [...this.added_users, user];
            }
          }
          console.log("USERS", users);
        });
      }
    }

    this.dealership_model = dealership;
  }

  addUser() {
    console.log('ADD USER', this.add_user);
    // if(this.dealership_uid) {
    //     this.projectManager.addUser(this.add_user , this.dealership_uid);
    // }
    this.added_users.push(this.add_user);
  }

  removeUser(idx: number) {
    if(this.added_users[idx].$key)
      this.removed_users.push(this.added_users[idx]);
    
    this.added_users = [
      ...this.added_users.slice(0, idx),
      ...this.added_users.slice(idx+1)
   ];
  }

  addDealership() {
    var ret_dealership = {
      $key: this.dealership_model.$key,
      data: {
        name: this.dealership_model.name,
        location: this.dealership_model.location,
        project_manager: this.dealership_model.project_manager
      }
    };

    if(this.dialogRef.config.data.role === 'add') {
      ret_dealership.data.project_manager = this.projectManager._uid;
    }

    var ret_add_products = this.products.filter(product => product.selected && product.op === 'ADD' && product.milestone_model).map(product => {
      var ret_dates: any = {}

      console.log("ADD PRODUCT", product);

      if(product.started && !isNaN(product.started)) {
        ret_dates.started = product.started.toDateString();
      } else {
        ret_dates.started = (new Date(Date.now())).toDateString();
      }

      if(product.activation && !isNaN(product.activation)) {
        ret_dates.activation = product.activation.toDateString();
      } else {
        ret_dates.activation = (new Date(Date.now() + (35 * 8.64e+7))).toDateString();
      }

      var ret  = Object.assign({
        name: product.name,
        product_type: product.product_type,
        type: getProductType(product.name),
        milestone_model: product.milestone_model
      }, ret_dates);

      return ret;
    });


    var ret_update_products = this.products.filter( (product, i) => product.selected && product.op === 'UPDATE' && product.milestone_model ).map(product => {
      var ret_dates: any = { };

      if(product.started && !isNaN((product.started  as Date).getMilliseconds() )) {
        ret_dates.started = product.started.toDateString();
      }

      if(product.activation && !isNaN( (product.activation as Date).getMilliseconds() )) {
        ret_dates.activation = product.activation.toDateString();
      }

      var ret  = {
        $key: product.$key,
        data: Object.assign({
          name: product.name,
          product_type: product.product_type,
          type: product.type,
          milestone_model: product.milestone_model
        }, ret_dates)
      }
      
      return ret;
    });

    var ret_remove_products = this.products.filter(product => product.op === 'REMOVE' && product.milestone_model).map(product => ({
      $key: product.$key
    }));

    var ret_update_users = this.added_users.filter( user => user.$key ).map(user => ({
      $key: user.$key,
      role: user.role || 'secondary',
      data : {
        name: user.name,
        email: user.email,
        phone: user.phone,
        group: 'customer',
        dealerships: user.dealerships || []
      }
    }));

    var ret_add_users = this.added_users.filter( user => user.$key === undefined || user.$key === null ).map(user => ({
      role: user.role  || 'secondary',
      data : {
        name: user.name,
        email: user.email,
        phone: user.phone,
        group: 'customer',
        dealerships: user.dealerships || []
      }
    }));
 
    var ret_remove_users = this.removed_users.map(user => ({
      $key: user.$key
    }));

    this.dialogRef.close({
      dealership: ret_dealership,
      products: {
        add: ret_add_products,
        update: ret_update_products,
        remove: ret_remove_products
      },
      users: {
        add: ret_add_users,
        update: ret_update_users,
        remove: ret_remove_users
      }
    });
  }

}
