import { Component, OnInit, Input } from '@angular/core';
import { MdSnackBar } from  '@angular/material';

import { copyToClipboard } from '../utils/utils';

@Component({
  selector: 'pt-label-to-clipboard',
  templateUrl: './pt-label-to-clipboard.component.html',
  styleUrls: ['./pt-label-to-clipboard.component.scss']
})
export class PtLabelToClipboardComponent implements OnInit {
  @Input() label: string = "";

  constructor(private snackBar: MdSnackBar) { }

  onClick() {
    if(this.label) {
      copyToClipboard(this.label)
      this.snackBar.open('Text copied to clipboard', "GOT IT", {
        duration: 1500
      });
    }
  }

  ngOnInit() {
  }

}
