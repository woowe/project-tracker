export function getIcon(alt: string): string {
    switch(alt) {
      case "DealerSocket": case "DealerSocket CRM":
        return "trending_up";
      case "DealerFire":
        return "dashboard";
      case "Inventory+":
        return "search";
      case "iDMS":
        return "directions_car";
      case "Revenue Radar":
        return "attach_money";
      default:
        break;
    }
}

export function getProductType(name: string) {
  switch(name) {
    case "DealerSocket": case "DealerSocket CRM":
      return 0;
    case "DealerFire":
      return 1;
    case "Inventory+":
      return 2;
    case "iDMS":
      return 3;
    case "Revenue Radar":
      return 4;
    default:
      return null;
  }
}

export function productNameEq(p1_name: string, p2_name: string) {
  if(p1_name.match(/DealerSocket/) && p2_name.match(/DealerSocket/)) {
    return true;
  }

  if(p1_name.match(/DealerFire/) && p2_name.match(/DealerFire/)) {
    return true;
  }

  if(p1_name.match(/Inventory\+/) && p2_name.match(/Inventory\+/)) {
    return true;
  }

  if(p1_name.match(/iDMS/) && p2_name.match(/iDMS/)) {
    return true;
  }

  if(p1_name.match(/Revenue Radar/) && p2_name.match(/Revenue Radar/)) {
    return true;
  }

  return false;
}

export function fuzzysearch (needle, haystack) {
  var hlen = haystack.length;
  var nlen = needle.length;
  if (nlen > hlen) {
    return false;
  }
  if (nlen === hlen) {
    return needle === haystack;
  }
  outer: for (var i = 0, j = 0; i < nlen; i++) {
    var nch = needle.charCodeAt(i);
    while (j < hlen) {
      if (haystack.charCodeAt(j++) === nch) {
        continue outer;
      }
    }
    return false;
  }
  return true;
}

export function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
  var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

  return {
    x: centerX + (radius * Math.cos(angleInRadians)),
    y: centerY + (radius * Math.sin(angleInRadians))
  };
}

export function describeArc(x, y, radius, startAngle, endAngle){

    var start = polarToCartesian(x, y, radius, endAngle);
    var end = polarToCartesian(x, y, radius, startAngle);

    var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

    var d = [
        "M", start.x, start.y,
        "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
    ].join(" ");

    return d;
}

export function parseDate(date: string) {
  if(typeof date !== 'string') {
    return null;
  }

  let d = Date.parse(date);

  if(isNaN(d)) {
    if(date.match(/(\d{2})\/(\d{2})\/(\d{4})/gi).length > 0) {
      let tmp = date.split('/').map(s => parseInt(s)).filter(s => s !== null || s !== undefined);
      if(tmp.length === 3) {
        var tmp_d = new Date(tmp[2] + '-' + tmp[1] + '-' + tmp[0]);
        d = Date.parse(tmp_d.toDateString());
      } else {
        return null;
      }
    }
  }

  return d;
}

/**
 * 
 */
export function getDayDiff(start_ms: number, end_ms: number, days_diff: number) {
  var days_diff_ms = Math.abs(days_diff) * 86400000;
  return (days_diff > 0) ? start_ms + days_diff_ms : end_ms - days_diff_ms;
}

/**
 * Calculates the completion of the product's milestones
 * @param p_info Product information
 * @param m_info Milestone information
 * @return object precent_complete: number in percent, status: 'Needs Attention' | 'On Schedule', need_attenion_milestones: array of milestones that are marked as 'Needs Attention'
 */
export function calculateMilestoneCompletion(p_info: any, m_info: any): any {
  if(typeof p_info !== "object" || typeof m_info !== "object") {
    return null;
  }
  // gets milliseconds then divides by 86400000 to convert milliseconds into days.
  var date_now = Date.now();
  var activation = Date.parse(p_info.activation);
  var started = Date.parse(p_info.started);

  if(!date_now || !activation || !started) {
    return null;
  }

  var total_time = (activation - started) / 86400000 | 0;
  var elapsed_time = (date_now - started) / 86400000 | 0;

  var need_attenion_milestones: Array<string> = [];

  var total_points = 0;
  var acc_points = 0;
  for(let milestone of m_info.milestones) {
    if(getDayDiff(started, activation, milestone.days_differential) <= date_now && milestone.status === "Complete") {
      acc_points += milestone.points;
    }
    if(milestone.status === "Needs Attention") {
      need_attenion_milestones.push(milestone.name);
    }
    total_points += milestone.points;
  }
  var percent_complete = acc_points / total_points;

  return {
    percent_complete: percent_complete * 100,
    status: need_attenion_milestones.length > 0 ? "Needs Attention" : "On Schedule",
    need_attenion_milestones
  };
}

/**
 * Check if two objects are equal
 * @param o1 fist object to compare
 * @param o2 second object to compare
 * @return boolean whether or not the objects are the same
 */
export function objectEq(o1, o2) {
  let o1_keys = Object.keys(o1);
  let o2_keys = Object.keys(o2);

  if(o1_keys.length !== o2_keys.length) {
    return false;
  }
  
  for(var i = 0; i < o1_keys.length; ++i) {
    if(o1_keys[i] !== o2_keys[i]) {
      return false;
    }
  }

  var eq = true;
  for(let key of o1_keys) {
    if(o1[key] !== o2[key]) {
      return false;
    }
  }

  return eq;
}

/**
 * get the differences in an array of objects
 * @param from Old object
 * @param to New object
 * @return object 
 */
export function diffObjArray(from, to) {
  
  let ret = {
    add: [],
    remove: []
  }
  
  var no_match = [];
  for(var i = 0; i < from.length; ++i)
    no_match.push(i);
  
  for(var i = 0; i < to.length; ++i) {
    var match_idx = -1;
    for(var j = 0; j < from.length; ++j) {
      if(objectEq(to[i], from[j])) {
        match_idx = j;
        break;
      }
    }
    
    if(match_idx > -1) {
      no_match[match_idx] = undefined;
    }
    else {
      ret.add.push(i);
    }
  }
  
  ret.remove = [...ret.remove, ...no_match.filter(m => m !== undefined)];
  
  return ret;
}

/**
 * format date
 * @param date Date Object
 * @return string with formatted date
 */
export function formatDate(date: Date, format: string): string {
  return format
    .replace('YYYY', date.getFullYear() + '')
    .replace('MM', prependZero((date.getMonth() + 1) + ''))
    .replace('DD', prependZero(date.getDate() + ''))
    .replace('HH', prependZero(date.getHours() + ''))
    .replace('mm', prependZero(date.getMinutes() + ''))
    .replace('ss', prependZero(date.getSeconds() + ''));
}

/**
 * Prepend Zero
 * @param value String value
 * @return string with prepend Zero
 */
export function prependZero(value: string): string {
  return parseInt(value) < 10 ? '0' + value : value;
}

// A simple function to copy a string to clipboard. See https://github.com/lgarron/clipboard.js for a full solution.
export var copyToClipboard = (function() {
  var _dataString = null;
  document.addEventListener("copy", function(e){
    if (_dataString !== null) {
      try {
        (e as any).clipboardData.setData("text/plain", _dataString);
        e.preventDefault();
      } finally {
        _dataString = null;
      }
    }
  });
  return function(data) {
    _dataString = data;
    document.execCommand("copy");
  };
})();

/**
 * Fire an event handler to the specified node. Event handlers can detect that the event was fired programatically
 * by testing for a 'synthetic=true' property on the event object
 * @param {HTMLNode} node The node to fire the event handler on.
 * @param {String} eventName The name of the event without the "on" (e.g., "focus")
 */
export function fireEvent(node, eventName) {
    // Make sure we use the ownerDocument from the provided node to avoid cross-window problems
    var doc;
    if (node.ownerDocument) {
        doc = node.ownerDocument;
    } else if (node.nodeType == 9){
        // the node may be the document itself, nodeType 9 = DOCUMENT_NODE
        doc = node;
    } else {
        throw new Error("Invalid node passed to fireEvent: " + node.id);
    }

     if (node.dispatchEvent) {
        // Gecko-style approach (now the standard) takes more work
        var eventClass = "";

        // Different events have different event classes.
        // If this switch statement can't map an eventName to an eventClass,
        // the event firing is going to fail.
        switch (eventName) {
            case "click": // Dispatching of 'click' appears to not work correctly in Safari. Use 'mousedown' or 'mouseup' instead.
            case "mousedown":
            case "mouseup":
                eventClass = "MouseEvents";
                break;

            case "focus":
            case "change":
            case "blur":
            case "select":
                eventClass = "HTMLEvents";
                break;

            default:
                throw "fireEvent: Couldn't find an event class for event '" + eventName + "'.";
        }
        var event = doc.createEvent(eventClass);
        event.initEvent(eventName, true, true); // All events created as bubbling and cancelable.

        event.synthetic = true; // allow detection of synthetic events
        // The second parameter says go ahead with the default action
        node.dispatchEvent(event, true);
    } else  if (node.fireEvent) {
        // IE-old school style
        var event = doc.createEventObject();
        event.synthetic = true; // allow detection of synthetic events
        node.fireEvent("on" + eventName, event);
    }
};

/**
 * Fancy ID generator that creates 20-character string identifiers with the following properties:
 *
 * 1. They're based on timestamp so that they sort *after* any existing ids.
 * 2. They contain 72-bits of random data after the timestamp so that IDs won't collide with other clients' IDs.
 * 3. They sort *lexicographically* (so the timestamp is converted to characters that will sort properly).
 * 4. They're monotonically increasing.  Even if you generate more than one in the same timestamp, the
 *    latter ones will sort after the former ones.  We do this by using the previous random bits
 *    but "incrementing" them by 1 (only in the case of a timestamp collision).
 */
export function generateUID() {
  // Modeled after base64 web-safe chars, but ordered by ASCII.
  var PUSH_CHARS = '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';

  // Timestamp of last push, used to prevent local collisions if you push twice in one ms.
  var lastPushTime = 0;

  // We generate 72-bits of randomness which get turned into 12 characters and appended to the
  // timestamp to prevent collisions with other clients.  We store the last characters we
  // generated because in the event of a collision, we'll use those same characters except
  // "incremented" by one.
  var lastRandChars = [];

  return function() {
    var now = new Date().getTime();
    var duplicateTime = (now === lastPushTime);
    lastPushTime = now;

    var timeStampChars = new Array(8);
    for (var i = 7; i >= 0; i--) {
      timeStampChars[i] = PUSH_CHARS.charAt(now % 64);
      // NOTE: Can't use << here because javascript will convert to int and lose the upper bits.
      now = Math.floor(now / 64);
    }
    if (now !== 0) throw new Error('We should have converted the entire timestamp.');

    var id = timeStampChars.join('');

    if (!duplicateTime) {
      for (i = 0; i < 12; i++) {
        lastRandChars[i] = Math.floor(Math.random() * 64);
      }
    } else {
      // If the timestamp hasn't changed since last push, use the same random number, except incremented by 1.
      for (i = 11; i >= 0 && lastRandChars[i] === 63; i--) {
        lastRandChars[i] = 0;
      }
      lastRandChars[i]++;
    }
    for (i = 0; i < 12; i++) {
      id += PUSH_CHARS.charAt(lastRandChars[i]);
    }
    if(id.length != 20) throw new Error('Length should be 20.');

    return id;
  };
}
