/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PtSvgMorphComponent } from './pt-svg-morph.component';

describe('PtSvgMorphComponent', () => {
  let component: PtSvgMorphComponent;
  let fixture: ComponentFixture<PtSvgMorphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtSvgMorphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtSvgMorphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
