import { Component, OnInit, ViewChild, ElementRef, Input, OnChanges, AfterViewInit, Renderer } from '@angular/core';

var SVG = require("svg.js");
require('svg.pathmorphing.js');

@Component({
  selector: 'div[pt-svg-morph]',
  styles: [
    `
    :host {
      display: flex;
      justify-content: center;
      align-items: center;
    }
    `
  ],
  template: `<svg id="ptSvgPath" #svgDoc></svg>`
})
export class PtSvgMorphComponent implements OnChanges, AfterViewInit {
  @ViewChild('svgDoc')
  svgDoc: ElementRef;

  @Input() paths: {name: string, path: string}[];
  @Input() selectedPath: string;
  @Input() svgDocOpts: any = {};

  draw: any;
  path: any;

  constructor(private renderer: Renderer) { }

  setAttributes() {
    // console.log('SETTING ATTRIBUTES');
    for(let opt_feild of Object.keys(this.svgDocOpts)) {
      this.renderer.setElementAttribute(this.svgDoc.nativeElement, opt_feild, this.svgDocOpts[opt_feild]);
    }
  }

  ngOnChanges(changes: any) {
    if(changes.selectedPath && SVG.supported && this.selectedPath && this.svgDoc.nativeElement && this.paths && this.draw) {
      // console.log('DOING CHANGE', changes, this.paths, this.svgDoc.nativeElement);
      if(changes.selectedPath.currentValue !== changes.selectedPath.previousValue) {
        var updated_path = this.paths.filter(el => el.name === changes.selectedPath.currentValue)[0].path;
        if(updated_path) {
          this.path.animate(150, "<>", 0).plot(updated_path);
        }
      }
    }
  }

  ngAfterViewInit() {
    if(!SVG.supported && this.svgDoc.nativeElement) {
      return;
    }

    // console.log('AFTER VIEW INIT', this.selectedPath);

    this.setAttributes();

    this.draw = SVG.adopt(this.svgDoc.nativeElement);

    var initial_path;
    for(let info of this.paths) {
      if(info.name === this.selectedPath){
        initial_path = info.path;
      }
    }

    if(initial_path) {
      this.path = this.draw.path(initial_path);
    }
  }
}
