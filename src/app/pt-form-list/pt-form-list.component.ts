import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';

import { Observable } from 'rxjs';

import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';

import * as ptValidator from '../validators/pt-validators';

@Component({
  selector: 'app-pt-form-list',
  templateUrl: './pt-form-list.component.html',
  styleUrls: ['./pt-form-list.component.scss']
})
export class PtFormListComponent implements OnInit {
  forms: any[] = [];
  loading: boolean = false;

  constructor(private pm: ProjectManagerService,
              private dialog: MdDialog, private snackBar: MdSnackBar) { }

  ngOnInit() {
    this.loading = true;
    Observable.from(this.pm.getAllForms())
      .subscribe( forms => {
        this.forms = forms;
        console.log('FORMS', forms);
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.loading = false;
      },
      (/*complete*/) => {
        this.loading = false;
      });
  }

  onRemove(form) {
    let dialog_ref = this.dialog.open(PtFormRemoveDialogComponent, {
      data: {
        form
      }
    });

    dialog_ref.afterClosed().subscribe( res => {
      if (res === 'REMOVE' && form) {
        this.snackBar.open('Removing form...');
        Observable.from(this.pm.removeFormST(form.name))
          .mergeMap(r => this.pm.removeFormDB(form.$key))
          .subscribe(r => {
            this.snackBar.open('Form Removed!', 'GOT IT', { duration: 1500 });
          },
          error => {
            this.snackBar.open('An error occured!', 'GOT IT', { duration: 1500 });
            console.error(error);
          });
      }
    })
  }
}

@Component({
  selector: 'pt-form-remove-dialog',
  template: `
  <h2 md-dialog-title>Remove Form</h2>
  <md-dialog-content>
    <p> Are you sure? </p>
  </md-dialog-content>
  <md-dialog-actions>
    <button md-button md-dialog-close>No</button>
    <button md-raised-button (click)="dialogRef.close('REMOVE')" color="warn">Yes</button>
  </md-dialog-actions>
  `,
  styles: [
    `
    h2, p {
      font-family: 'Roboto', sans-serif;
    }
    `
  ]
})
export class PtFormRemoveDialogComponent implements OnInit {
  constructor(public dialogRef: MdDialogRef<PtFormRemoveDialogComponent>) {
  }

  ngOnInit() {
  }
}