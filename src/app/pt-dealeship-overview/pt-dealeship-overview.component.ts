import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MdDialog, MdSnackBar } from '@angular/material';

import { PmDashboardService } from '../services/PmDashboard/pm-dashboard.service';
import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';
import { UserInfoService } from '../services/UserInfo/user-info.service';
import { ProductLogosService } from '../services/ProductLogos/product-logos.service';
import { calculateMilestoneCompletion, getIcon, objectEq } from '../utils/utils';

import { AddDealershipDialog } from '../project-manager-dashboard/project-manager-dashboard.component';

import { Observable } from 'rxjs';

@Component({
  selector: 'pt-dealeship-overview',
  templateUrl: './pt-dealeship-overview.component.html',
  styleUrls: ['./pt-dealeship-overview.component.scss']
})
export class PtDealeshipOverviewComponent implements OnInit {

  dealerships: any = {};
  product_logos: any[] = [];
  tab: number = 0;
  obs: any;

  loaded = false;

  constructor(private pm: ProjectManagerService, private userInfo: UserInfoService,
              private pmdash: PmDashboardService, private pl: ProductLogosService,
              private dialog: MdDialog, private snack: MdSnackBar,
              private cdRef: ChangeDetectorRef) { }
  
  getStatus(dealership) {
    if(!dealership) {
      return '';
    }

    if(!dealership.products) {
      return '';
    }

    if(dealership.products.length === 0) {
      return '';
    }

    for(let products of dealership.products) {
      if(products.completion_info.status === 'Needs Attention') {
        return 'Needs Attention';
      }
    }

    return 'On Schedule';
  }

  getKeys(obj: any) {
    return obj ? Object.keys(obj) : null;
  }

  getIcon(alt: string) {
    return alt ? getIcon(alt): null;
  }

  objectEq(o1, o2) {
    return o1 && o2 ? objectEq(o1, o2): null;
  }

  onEdit(uid: string) {
    if(this.dealerships[uid]) {
      let dialogRef = this.dialog.open(AddDealershipDialog, {
        data: {
          role: 'edit',
          dealership: this.dealerships[uid]
        }
      });

      dialogRef.afterClosed().subscribe((res) => {
        console.log("AFTER CLOSE", res)
        if(res) {
          this.snack.open("Updating dealership info...", "GOT IT");

          var dealership = res.dealership;

          Observable.from(this.pm.updateDealershipInfo(dealership.$key, dealership.data))
            .first()
            .do( r => console.log('Done updating dealership'))
            .switchMap( r => 
              Observable.from(res.products.update)
                .mergeMap( ({$key, data}) => this.pm.updateProductBuilding($key, data))
                .materialize()
            )
            .do( r => console.log('Done updating dealership products', r))
            .first()
            .switchMap( r => 
              Observable.from(res.products.remove)
                .mergeMap( ({$key}) => this.pm.removeProductBuildingFromDealership(dealership.$key, $key))
                .materialize()
            )
            .first()
            // .do( r => console.log('Done removing dealership products'))
            .switchMap( r => 
              Observable.from(res.products.add)
                .mergeMap( (product) => Observable.from(this.pm.addProductBuildingToDealership(dealership.$key, product)))
                .materialize()
            )
            .first()
            // .do( r => console.log('Done adding dealership products'))
            .switchMap( r => 
              Observable.from(res.users.update)
                // .do( ({$key, role, data}) => console.log('UPDATE USER', $key, role, data))
                .mergeMap( ({$key, role, data}) => this.pm.addUserToDealership(role, $key, dealership.$key), (user, r) => user)
                .mergeMap( ({$key, role, data}) => this.pm.addDealershipToUser($key, dealership.$key))
                .materialize()
            )
            .first()
            // .do( r => console.log('Done updating dealership users'))
            .switchMap( r => 
              Observable.from(res.users.add)
                .mergeMap( ({role, data}) => this.pm.createCustomer(data), ({role, data}, r) => ({$key: r.uid, role, data}))
                .mergeMap( ({$key, role, data}) => this.pm.addUserToDealership(role, $key, dealership.$key), (user, r) => user)
                .mergeMap( ({$key, role, data}) => this.pm.addDealershipToUser($key, dealership.$key))
                .materialize()
            )
            .first()
            // .do( r => console.log('Done adding dealership users'))
            .switchMap( r => 
              Observable.from(res.users.remove)
                .mergeMap( ({$key}) => this.pm.removeCustomerFromDealership($key, dealership.$key))
                .materialize()
            )
            .first()
            // .do( r => console.log('Done removing dealership users'))
            .subscribe(
              (next) => {
                // console.log("UPDATE", next);
              },
              (err) => {
                this.snack.open("Error updating dealership info!", "GOT IT", { duration: 1500 });
              },
              () => {
                console.log("COMPLETED")
                this.snack.open("Updated dealership info!", "GOT IT", { duration: 1500 });
              });
        }
      });
    }
  }

  onAdd() {

    let dialogRef = this.dialog.open(AddDealershipDialog, {
      data: {
        role: 'add',
        dealership: null
      }
    });

    dialogRef.afterClosed().subscribe((res) => {
      console.log("AFTER CLOSE", res)

      if(res) {
          this.snack.open("Adding dealership...", "GOT IT", { duration: 1500 });

          var dealership = res.dealership;

          console.log('ADD DEALERSHIP', res.dealership);
          

          Observable.from(this.pm.addDealership(dealership.data))
            .first()
            .do( r => console.log('Done updating dealership', r))
            .switchMap( ref => 
              Observable.from(res.products.add)
                .mergeMap( (product) => Observable.from(this.pm.addProductBuildingToDealership(ref.path.o[1], product)))
                .materialize()
              , (ref, r) => ref
            )
            .first()
            .do( r => console.log('Done adding dealership products'))
            .switchMap( ref => 
              Observable.from(res.users.update)
                .mergeMap( ({$key, role, data}) => this.pm.addUserToDealership(role, $key, ref.path.o[1]), (user, r) => user)
                .mergeMap( ({$key, role, data}) => this.pm.addDealershipToUser($key, ref.path.o[1]))
                .materialize()
              , (ref, r) => ref
            )
            .first()
            .do( r => console.log('Done updating dealership users'))
            .subscribe(
              (next) => {
                // console.log("UPDATE", next);
              },
              (err) => {

              },
              () => {
                console.log("COMPLETED")
                this.snack.open("Added dealership!", "GOT IT", { duration: 1500 });
              });
        }
    });
  }

  ngOnInit() {
    this.obs = Observable.from(this.userInfo.auth)
      .first()
      // .do( r => console.log('bloo'))
      .mergeMap(auth => this.pm.getProjectManagerInfo(auth))
      // .do( r => console.log('Blee'))
      .map((dealership, index) => {
        // console.log('MAP DEALER', dealership, index, this.pm._num_dealers);
        if(index >= this.pm._num_dealers) {
          this.dealerships = {};
          throw new Error('error');
        }
        return (dealership);
      })
      // .do( r => console.log('BLAH'))
      .retry(99999);

    this.obs.subscribe(res => {
        if(!this.dealerships) {
          this.dealerships = {};
        }

        this.loaded = true;

        if(res === null || res === undefined) { return; }

        if(res.$key) {
          if(res.products) {
            for(let product of res.products) {
              product.completion_info = calculateMilestoneCompletion(product, product.milestone_model);
            }
          } else {
            this.dealerships[res.$key].products = null;
          }
          this.dealerships[res.$key] = res;
        }

        
        // this.cdRef.detectChanges();
        // console.log("OVERVIEW", res, this.dealerships);
      },
      (error) => {
        console.log("ERROR LOADING PM INFO", error);
        this.loaded = true;
      },
      (/*completed*/) => {
        this.loaded = true;
        console.log("COMPLETED");
      });
  }

  ngOnDestroy() {
    this.obs.unsubscribe();
  }

}
