import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { MdSnackBar } from '@angular/material';

import { AngularFire } from 'angularfire2';
import * as firebase from 'firebase';

import { fireEvent } from '../utils/utils';

@Component({
  selector: 'pt-upload-pdf',
  template: `
    <div class="pt-upload-container">
      <input class="pt-upload-button" md-raised-button type="file" id="filebutton" value="upload" #filebtn (change)="filebuttoni($event)">
      <button md-raised-button color="primary" (click)="onClick()">UPLOAD PDF</button>
      <md-progress-bar
          class="pt-progress-bar"
          [color]="color"
          mode="determinate"
          [value]="progress">
      </md-progress-bar>
    </div>
    `,
    styles: [`
    .pt-upload-button {
      display: none;
    }

    md-progress-bar {
      margin-top: 10px;
    }
    `]
})
export class PtUploadPdfComponent {
  @ViewChild('filebtn') uploader;
  color = 'primary';

  storageref;
  storage;
  path;

  progress = 0;

  forms: any[] = null;

  constructor(private snackBar: MdSnackBar, private af: AngularFire,
              private _ngZone: NgZone){
    this.storage = firebase.storage().ref();

    this.af.database.list('/Forms').subscribe( forms => {
      this.forms = forms;
    });

  }

  reset() {
    this.color = 'primary';
    this.progress = 0;
    // this.ref.detectChanges();
  }

  onClick() {
    fireEvent(this.uploader.nativeElement, 'click');
  }
  
  filebuttoni(event){
    event.stopPropagation();
    event.preventDefault();

    let files = event.srcElement.files[0];

    if(this.forms) {
      var form = this.forms.find(f => f.name === files.name);

      if(form) {
        this.snackBar.open('There is a form with that name already', '', {
          duration: 1500
        });

        return;
      }

      // let uploader=document.getElementById("uploader");
      this.path="Forms/"+files.name;
      this.storageref = this.storage.child(this.path);
      let metadata = {
        contentDisposition: 'attachment'
      };

      let task = this.storageref.put(files, metadata);

      this.snackBar.open('Uploading file..');

      task.on('state_changed', (snapshot) => {
        this.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      (error) => {
        this.snackBar.open('Error uploading file.', 'GOT IT', {
          duration: 1500
        });
        
        this.color = 'warn';

        setTimeout(() => {
          this._ngZone.run( () => this.reset() )
        }, 1000);
      },
      (/*complete*/) => {
        this.snackBar.open('Upload complete!', 'GOT IT', {
          duration: 1500
        });

        setTimeout(() => {
          this._ngZone.run( () => this.reset() )
        }, 1000);
      });

      task.then( (snapshot) => {
        
        let save_obj = {
          downloadUrl: snapshot.downloadURL,
          name: snapshot.metadata.name,
          updated: snapshot.metadata.updated
        };
        console.log("SNAPSHOT", snapshot, save_obj);
        this.af.database.list('/Forms').push(save_obj);

      });

    } else {
      this.snackBar.open('Unable to upload, please try again', 'OKAY', {
        duration: 1500
      });
    }
  }

}
