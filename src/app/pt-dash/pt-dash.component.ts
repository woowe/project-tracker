import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  HostListener,
  Input
} from '@angular/core';

@Component({
  selector: 'pt-dash-menu',
  template: `<ng-content></ng-content>`,
  styles: [
    `
    :host {
      display: flex;
      flex-direction: column;
      min-height: 100%;
      overflow: hidden;
    }
    `
  ]
})
export class PtDashMenuComponent {}

@Component({
  selector: 'pt-dash',
  templateUrl: './pt-dash.component.html',
  styleUrls: ['./pt-dash.component.scss']
})
export class PtDashComponent implements OnInit, AfterViewInit {
  @ViewChild('sideNav')
  sidenav: ElementRef;

  @ViewChild('sideNavContainer')
  sideNavContainer: ElementRef;

  sidenav_button_state = "open";
  sidenav_can_lock = true;
  sidenav_mode = "side";
  pinned_icon = "lock_open";
  pinned_state = "unpinned";

  @Input() storageNamespace: string = "PSC";
  @Input() pinBreakpoint: number = 1500;
  @Input() arrowFill: string = "#fff";
  @Input() lockFill: string = "#fff";

  constructor() {}

  onSidenavOpenStart() {
    this.sidenav_button_state = "close";
  }

  onSidenavCloseStart() {
    this.sidenav_button_state = "open";
  }

  togglePinndedIcon() {
    if(this.pinned_icon === "lock_open") {
      this.pinned_icon = "lock";
      this.pinned_state = "pinned";
    } else {
      this.pinned_icon = "lock_open";
      this.pinned_state = "unpinned";
    }
  }

  ngOnInit() {
    let storage_obj = localStorage.getItem(`${this.storageNamespace}`);

    if(storage_obj) {
      storage_obj = JSON.parse(storage_obj);

      for(let feild of Object.keys(storage_obj)) {
        if(storage_obj[feild])
          this[feild] = storage_obj[feild];
      }

      if(this.pinned_state === "pinned" && window.innerWidth >= this.pinBreakpoint) {
        (this.sidenav as any).opened = true;
        this.sidenav_button_state = "close";
        this.pinned_icon = "lock";
      }
    }
  }

  @HostListener('window:unload', ['$event'])
  saveState() {
    let storage_obj: any = {};
    
    if(this.pinned_state !== "unpinned") {
      storage_obj.pinned_state = this.pinned_state;
    }

    localStorage.setItem(`${this.storageNamespace}`, JSON.stringify(storage_obj));
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.sidenav_mode = "side";
    this.sidenav_can_lock = true;
    if(window.innerWidth < this.pinBreakpoint) {
      this.sidenav_mode = "over";
      this.sidenav_can_lock = false;
      this.pinned_state = "unpinned";
      this.pinned_icon = "lock_open";
    }

    var el = (this.sideNavContainer as any)._element.nativeElement;
    if(el) {
      var top = el.getBoundingClientRect().top;
      el.style.minHeight = (window.innerHeight - top) + 'px';
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.onResize();
    }, 550);
  }

  ngOnDestroy() {
    this.saveState();
  }
}
