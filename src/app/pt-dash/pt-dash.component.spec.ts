/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PtDashComponent } from './pt-dash.component';

describe('PtDashComponent', () => {
  let component: PtDashComponent;
  let fixture: ComponentFixture<PtDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtDashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
