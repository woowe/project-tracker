import {
  Component,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate,
  ViewChild,
  ElementRef,
  AfterContentInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';

import { Observable } from 'rxjs';

import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';
import { UserInfoService } from '../services/UserInfo/user-info.service';
import { fuzzysearch } from '../utils/utils';
import * as ptValidators from '../validators/pt-validators';

import { MilestoneEditorComponent } from '../milestone-editor/milestone-editor.component';


@Component({
  selector: 'app-pt-milestone-template-list',
  templateUrl: './pt-milestone-template-list.component.html',
  styleUrls: ['./pt-milestone-template-list.component.scss'],
  animations: [
    trigger('expandState', [
      state('out', style({height: 0})),
      state('in', style({height: "*"})),
      transition("in => out", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
      transition("out => in", animate("0.45s cubic-bezier(.4, 0, .2, 1)"))
    ]),
    trigger('collapserState', [
      state('expand', style({transform: 'rotateZ(180deg)'})),
      state('collapse', style({transform: 'rotateZ(0deg)'})),
      transition("expand <=> collapse", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
    ])
  ]
})
export class PtMilestoneTemplateListComponent implements OnInit, AfterContentInit {
  @ViewChild('query')
  search_input: ElementRef;

  info: any = { selected: {} };

  milestones: any[];

  milestones$: Observable<any[]>;
  filtered_milestones$: Observable<any[]>;

  constructor(private pm: ProjectManagerService, private userInfo: UserInfoService,
              private dialog: MdDialog, public snackBar: MdSnackBar) { }

  filterMilestones(query: string, milestones) {
    if(!query) {
      return milestones;
    }

    var filtered = [];
    for(let milestone of milestones) {
      if(fuzzysearch(query, milestone.name))
        filtered.push(milestone);
    }
    return filtered;
  }

  onEdit(milestone) {
    let dialog_ref = this.dialog.open(MilestoneEditorComponent, {
      data: {
        milestone_model: milestone
      }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED EDIT", res);
      this.save(milestone.$key, res);
    })
  }

  onAdd() {
    let dialog_ref = this.dialog.open(MilestoneEditorComponent, {
      data: {
        milestone_model: {
          name: '',
          milestones: []
        }
      }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED ADD", res);
      this.save(null, res);
    })
  }

  onRemove(milestone) {
    let dialog_ref = this.dialog.open(PtRemoveMilestoneTemplateDialog, {
      data: { milestone }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED REMOVE", res);
    })
  }

  save(m_uid: string, milestone: any) {
    if(!milestone) { return; }
    if(m_uid) {
      this.snackBar.open(`Updating`, '', { duration: 2000 });
      this.pm.updateMilestoneTemplate(m_uid, milestone).then(() => {
        this.snackBar.open(`Updated!`, '', { duration: 2000 });
      });
    } else {
      this.snackBar.open(`Adding`, '', { duration: 2000 });
      this.pm.addMilestoneTemplate(milestone).then(() => {
        this.snackBar.open(`Added!`, '', { duration: 2000 });
      });
    }
  }

  toggleExpanded(key: string) {
    var e = this.info.expanded[key];
    if(e) {
      this.info.expanded[key] = false;
    } else {
      this.info.expanded[key] = true;
    }
  }

  ngOnInit() {
    this.milestones$ = Observable.from(this.userInfo.auth)
      .first()
      .mergeMap(auth => this.pm.getAllMilestoneTemplates());
  }

  ngAfterContentInit() {
    this.filtered_milestones$ = Observable.fromEvent(this.search_input.nativeElement, 'input')
      .startWith({ target: { value: '' }})
      .mergeMap( (event: any) => this.milestones$, (event, milestones) => ({event, milestones}) )
      .map( ({event, milestones}) => event ? this.filterMilestones(event.target.value, milestones) : milestones );
  }

}

@Component({
  selector: 'pt-remove-milestone-template-dialog',
  styles: [
    `
    h2, p {
      font-family: 'Roboto', sans-serif;
    }
    `
  ],
  template: `
  <h2 md-dialog-title>Remove Milestone</h2>
  <md-dialog-content>
    <p> Are you sure? </p>
  </md-dialog-content>
  <md-dialog-actions>
    <button md-button md-dialog-close>No</button>
    <button md-raised-button (click)="onRemove()" color="warn">Yes</button>
  </md-dialog-actions>
  `
})
export class PtRemoveMilestoneTemplateDialog implements OnInit  {

  constructor(public dialogRef: MdDialogRef<PtRemoveMilestoneTemplateDialog>, private pm: ProjectManagerService,
              public snackBar: MdSnackBar) {
  }

  ngOnInit() {
    console.log(this.dialogRef.config.data);
  }

  onRemove() {
    if(this.dialogRef.config.data.milestone) {
      this.snackBar.open(`Removing`, '', { duration: 2000 });
      this.pm.removeMilestoneTemplate(this.dialogRef.config.data.milestone.$key).then(milestone => {
        this.snackBar.open(`Removed`, '', { duration: 2000 });
      });
      this.dialogRef.close();
    }
  }
}
