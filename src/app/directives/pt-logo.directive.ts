import { Directive, Input, ElementRef } from '@angular/core';

interface PtLogo {
  alt: string,
  url: string,
  path: string
}

@Directive({
  selector: 'img[ptLogo]'
})
export class PtLogoDirective {
  constructor(private _el: ElementRef) { }

  @Input() set ptLogo(ptLogo: PtLogo) {
    if(!ptLogo) { return; }
    
    if(ptLogo.path && ptLogo.alt) {
      this._el.nativeElement.src = ptLogo.path;
      this._el.nativeElement.alt = ptLogo.alt;
    }
    // console.log('PT LOGO ELEMENT', ptLogo, this._el.nativeElement.src, this._el.nativeElement.alt);
  }
}
