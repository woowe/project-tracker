import { NgModule } from '@angular/core';

import { PtLogoDirective } from './pt-logo.directive';

export const DIRECTIVES_DECLARATIONS = [
    PtLogoDirective
];

@NgModule({
    declarations: [ DIRECTIVES_DECLARATIONS ],
    exports: [ DIRECTIVES_DECLARATIONS ]
})
export class DirectivesModule {

}