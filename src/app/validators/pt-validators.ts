import { AbstractControl, AsyncValidatorFn  } from '@angular/forms';
import { Observable } from 'rxjs';
import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';

export function phoneValidator(c: AbstractControl) {
  if (c.value === "") { return null; }
  var phone_re = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
  return phone_re.test(c.value) ?  null : { 'valid_phone': false };
}

export function emailValidator(c: AbstractControl) {
  if (c.value === "") { return null; }
  var email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return email_re.test(c.value) ?  null : { 'valid_email': false };
}

export function urlValidator(c: AbstractControl) {
  if(c.value === "" ) { return null; }
  var url_re = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;
  return url_re.test(c.value) ? null : { 'valid_url': false };
}

export function formValidator(forms: any[]) {
  return (c: AbstractControl) => {
    var form = forms.find(f => f.$key === c.value);
    return form ? null : { 'form_exsits': false};
  }
}

export function userExistsValidatorFactory(pm: ProjectManagerService, except?: string[]): AsyncValidatorFn {
  if(!except)
    except = [];
  except = except.filter(e => e !== null || e !== undefined);
  
  return (c: AbstractControl) => {
    return pm.getUser(c.value)
      .map(users => users.filter(user => except.indexOf(user.email) === -1))
      // .do(users => console.log('FOUND USER', users))
      .map( r =>  r.length >= 1 ? { 'user_exsits': true} : null )
  };
}

export function productTemplateExsists(pm: ProjectManagerService, except?: string[]): AsyncValidatorFn {
  if(!except)
    except = [];
  except = except.filter(e => e !== null || e !== undefined);

  return (c: AbstractControl) => {
    return pm.getProductTemplatesByName(c.value)
      .map(name => name.filter(n => except.indexOf(n.name) === -1))
      // .do(({uid, name}) => console.log('FOUND MILESTONES', uid, name))
      .map( name =>  name.length >= 1 ? { 'product_template_exsits': true}  : null );
  };
}

export function milestoneTemplateExsists(pm: ProjectManagerService, errorOnExists = true, except?: string[]): AsyncValidatorFn {
  if(!except)
    except = [];
  except = except.filter(e => e !== null || e !== undefined);
  // console.log("EXCEPT", except);
  return (c: AbstractControl) => {
    return Observable.from(pm.getMilestoneTemplatesByUid(c.value))
      .mergeMap(uid => Observable.of(uid.filter(milestone => except.indexOf(milestone.name) === -1)) )
      .mergeMap(uid => pm.getMilestoneTemplatesByName(c.value), (uid,name) => ({uid, name}) )
      .mergeMap( ({uid, name}) => Observable.of({uid, name: name.filter(milestone => except.indexOf(milestone.name) === -1)}) )
      // .do(({uid, name}) => console.log('FOUND MILESTONES', uid, name))
      .map( ({uid, name}) =>  uid.length >= 1 || name.length >= 1 ? 
        (errorOnExists ? { 'milestone_template_exsits': true} : null) : 
        (!errorOnExists ? { 'milestone_template_exsits': false} : null) );
  };
}

export function unsignedGteOneValidator(c: AbstractControl) {
  var p_res = parseInt(c.value);
  if(p_res) {
    return p_res > 0 ? null : { 'invalid_number': true };
  }
  return { 'invalid_number': true };
}
