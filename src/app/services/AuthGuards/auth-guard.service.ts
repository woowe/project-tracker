import { Injectable, Inject } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { UserInfoService } from '../UserInfo/user-info.service';

import { environment } from '../../../environments/environment';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private user_info: UserInfoService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log(state.url);
    if(!this.user_info.loggedIn) {

      if( environment.type === 'dev' ) {
        // this.user_info.auth.logout();
        var cust_routes: Array<string> = ["/product-selection", "/milestone-tracker"];
        var pm_routes: Array<string> = ["/project-manager"];
        var username = "test_pm@dealersocket.com";
        var password = "projecttracker";
        if(cust_routes.filter(r => state.url.indexOf(r) > -1).length === 1) {
          username = "jason.brogrammer@gmail.com";
          password = "projecttracker";
        }
        
        this.user_info.login(username, password,
        success => {
          console.log("Firebase success with AuthGuard login", username);
        },
        error => {
          console.log("Firebase error with AuthGuard login");
        });

        return true;
      }

      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

}
