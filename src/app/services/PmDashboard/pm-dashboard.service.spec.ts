/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PmDashboardService } from './pm-dashboard.service';

describe('PmDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PmDashboardService]
    });
  });

  it('should ...', inject([PmDashboardService], (service: PmDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
