import { Injectable } from '@angular/core';

// import * as Firebase from 'firebase';
import { AngularFire, AuthProviders, AuthMethods, AngularFireAuth, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import * as firebase from "firebase";
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';

import { Observable, Subject, BehaviorSubject } from "rxjs/Rx";

import { UserInfoService } from '../UserInfo/user-info.service';
import { generateUID } from '../../utils/utils';

@Injectable()
export class ProjectManagerService {
  _info: any;

  _dealerships: any = {};
  _users: any;

  _uid: string;

  _num_dealers: number = 0;

  firebaseAuth: AngularFireAuth;
  // firebase: Firebase;

  constructor(private af: AngularFire, private http: Http, private userInfo: UserInfoService) {
    this.firebaseAuth = af.auth;
    this._users = this.af.database.list('/Users/');
  }

  getProjectManagerInfo(auth): Observable<any> {
    // console.log("RECIVED AUTH", auth);
    // return Observable.empty();
    return Observable.from(this.af.database.object(`/Users/${auth.uid}`))
      // .do(info => console.log("USER", info))
      .filter(info => info.group == "project manager")
      .do(info => {
        // console.log("PM UPDATE: ", info);
        this._info = info;
        this._uid = auth.uid;
      })
      .switchMap(info => {
        // make array in the [key, value] pairs
        let keys = info.dealerships ? Object.keys(info.dealerships) : [];
        this._num_dealers = Math.max(1, keys.length);
        console.log('INFO', info, info.dealerships);
        if (keys.length === 0) {
          return Observable.of(null);
        }

        

        return Observable.from(keys)
          .mergeMap( dealership_uid => this.af.database.object(`/Dealerships/${dealership_uid}`)
            , (dealership_uid, dealership) => dealership )
          .mergeMap( dealership =>
            Observable.combineLatest(
              Observable.from(Object.keys(dealership.users || {"*": "*"}))
                .filter( key => key !== "*" )
                .map( key => Observable.from(this.af.database.object(`/Users/${key}`))
                  .map( user => Object.assign(user, {role: dealership.users[key]}) )
                ).combineAll()
                .map(arr => (arr as any[]).filter( user => user.$key !== undefined && user.$value !== null))
                .materialize(),
              Observable.from(Object.keys(dealership.products || {"*": "*"}))
                .filter( key => key !== "*" )
                .map( key => Observable.from(this.af.database.object(`/Product Building/${key}`))
                ).combineAll()
                .map(arr => (arr as any[]).filter( product => product.$key !== undefined && product.$value !== null))
                .materialize(),
              (users, products) => {
                // console.log("USERS AND PRODUCTS", users, products)
                dealership.users = users.value || [];
                dealership.products = products.value || [];
                return dealership;
              })
            )
      });
  }

  get info(): any {
    return this._info;
  }

  get dealerships(): any {
    return this._dealerships;
  }

  /** GET */
  getAllProductTemplates() {
    return this.af.database.list('/Product Templates/');
  }
  getAllMilestoneTemplates() {
    return this.af.database.list('/Milestone Templates/');
  }
  getMilestoneTemplate(uid: string) {
    return this.af.database.object(`/Milestone Templates/${uid}`);
  }
  getAllUsers() {
    return this.af.database.list('/Users/');
  }
  getAllCustomers() {
    return Observable.from(this.getAllUsers()).map(users => users.filter(user => user.group === 'customer'));
  }
  getAllDealerships() {
    return this.af.database.list('/Dealerships');
  }

  getUser(email: string): Observable<any> {
    return Observable.from( this.af.database.list('/Users', {
      query: {
        orderByChild: 'email',
        equalTo: email,
      }
    }) ).first();
  }

  getMilestoneTemplatesByUid(m_uid: string) {
    return Observable.from( this.af.database.list('/Milestone Templates', {
      query: {
        orderByKey: true,
        equalTo: m_uid,
      }
    }) ).first();
  }

  getMilestoneTemplatesByName(name: string) {
    return Observable.from( this.af.database.list('/Milestone Templates', {
      query: {
        orderByChild: 'name',
        equalTo: name,
      }
    }) ).first();
  }

  getProductTemplatesByName(name: string) {
    return Observable.from( this.af.database.list('/Product Templates', {
      query: {
        orderByChild: 'name',
        equalTo: name,
      }
    }) ).first();
  }

  getUserByUid(uid: string) {
    return this.af.database.object(`/Users/${uid}`);
  }

  getDealershipProducts(d_uid: string) {
    return Observable.from(this.af.database.object(`/Dealerships/${d_uid}/products`))
      .mergeMap( products => Observable.from(Object.keys(products))
        .map( p_uid => 
          Observable.from(this.af.database.object(`/Product Building/${p_uid}`))
        ).combineAll()
      );
  }

  getDealershipUsers(d_uid: string) {
    return Observable.from(this.af.database.object(`/Dealerships/${d_uid}/users`))
      .mergeMap( users => Observable.from(Object.keys(users))
        .map( u_uid => 
          Observable.from(this.af.database.object(`/Users/${u_uid}`))
            .map( user => Object.assign(user, { role: users[u_uid] }) )
        ).combineAll()
      );
  }

  getAllForms() {
    return this.af.database.list('/Forms');
  }

  /** ADD */
  createCustomer(user_obj: any) {
    var password = generateUID()();
    return Observable.from(this.firebaseAuth.createUser({
        email: user_obj.email,
        password
      }))
      .filter( r => r !== null && r !== undefined)
      .mergeMap( r => {
        // console.log("AFTER CREATE USER", r);
        firebase.auth().sendPasswordResetEmail(user_obj.email);
        return this.addCustomer(user_obj, r.uid);
      }, (r, user) => r)
      .do( () => this.userInfo.reauth());
  }

  addDealership(dealership_obj) {
    return Observable.from(this.af.database.list('/Dealerships/').push(dealership_obj))
      .mergeMap(ref => this.af.database.object(`/Users/${this._uid}/dealerships/${ref.path.o[1]}`).set(true), (ref, r) => ref);
  }

  addDealershipToPm(dealership_uid) {
    return this.af.database.object(`/Users/${this._uid}/dealerships/${dealership_uid}`).update(dealership_uid);
  }

  addCustomer(user_obj: any, user_uid: string) {
    let user = Object.assign(user_obj, { group: 'customer' });

    return this.af.database.object(`/Users/${user_uid}`).set(user_obj);
  }

  addDealershipToUser(user_uid, dealership_uid) {
    return this.af.database.object(`/Users/${user_uid}/dealerships/${dealership_uid}`).set(true);
  }

  addUserToDealership(role: string, user_uid: string, dealership_uid: string) {
    return this.af.database.object(`/Dealerships/${dealership_uid}/users/${user_uid}`).set(role);
  }

  addMilestoneTemplate(milestone_obj: any) {
    return this.af.database.list('/Milestone Templates/').push(milestone_obj);
  }

  addMilestoneBuilding(milestone_obj: any) {
    return this.af.database.list('/Milestone Building/').push(milestone_obj);
  }

  addProductTemplate(product_obj: any) {
    return this.af.database.list('/Product Templates').push(product_obj);
  }

  addProductBuildingToDealership(dealership_uid: string, product_obj: any) {
    return this.af.database.list('/Product Building/').push(product_obj).then(ref => {
      return this.af.database.object(`/Dealerships/${dealership_uid}/products/${ref.path.o[1]}`).set(true);
    });
  }

  addForm(form_obj: any) {
    return this.af.database.list('/Forms').push(form_obj);
  }
  
  /** UPDATE */
  updatePmInfo(updated_info: any) {
    return this.af.database.object(`/Users/${this._uid}`).update(updated_info);
  }

  updateUserInfo(user_uid: string, updated_info: any) {
    return this.af.database.object(`/Users/${user_uid}`).update(updated_info);
  }

  updateMilestoneTemplate(m_uid: string, update_obj: any) {
    return this.af.database.object(`/Milestone Templates/${m_uid}`).update(update_obj);
  }

  updateProductTemplate(p_uid: string, update_obj: any) {
    return this.af.database.object(`/Product Templates/${p_uid}`).update(update_obj);
  }

  updateProductBuilding(p_uid: string, update_obj: any) {
    return this.af.database.object(`/Product Building/${p_uid}`).update(update_obj);
  }

  updateDealershipInfo(d_uid: string, update_obj: any) {
    return this.af.database.object(`/Dealerships/${d_uid}`).update(update_obj);
  }

  /** REMOVE */
  removeMilestoneTemplate(m_uid: string) {
    return this.af.database.object(`/Milestone Templates/${m_uid}`).remove();
  }

  removeProductTemplate(p_uid: string) {
    return this.af.database.object(`/Product Templates/${p_uid}`).remove();
  }

  removeProductBuildingFromDealership(d_uid: string, p_uid: string) {
    return Observable.from(this.af.database.object(`/Dealerships/${d_uid}/products/${p_uid}`).remove())
      .mergeMap(r => this.af.database.object(`/Product Building/${p_uid}`).remove());
  }

  removeUserViaBackend(uid: string) {
    return Observable.from(firebase.auth().currentUser.getToken(/* forceRefresh */ true))
      .mergeMap((idToken) => 
        this.http.get(`http://54.82.251.167:4223/api/users/delete/${idToken}/${environment.type}/${uid}`)
        .map((res: Response) => res.json())
        .mergeMap(res => {
          console.log('RESPONSE', res)
          if(res.auth && res.deleted) {
            return Observable.of(1);
          }
          return Observable.throw('error occurred in backend');
        })
      );
  }

  removeCustomer(user_uid: string) {
    return Observable.from(this.af.database.object(`/Users/${user_uid}`))
      .first()
      .mergeMap(user => this.removeUserViaBackend(user_uid), (user, r) => (user))
      .mergeMap(user => this.af.database.list(`/Users/${user_uid}`).remove(), (user, r) => (user))
      .filter(user => user.dealerships)
      .mergeMap(user =>
        Observable.from(Object.keys(user.dealerships))
          .mergeMap(d_uid => Observable.from(this.removeCustomerFromDealership(user_uid, d_uid)))
      , (user, r) => (user));
  }

  removeCustomerFromDealership(user_uid: string, dealership_uid: string) {
    return Observable.from(this.af.database.list(`/Dealerships/${dealership_uid}/users/${user_uid}`).remove())
          .mergeMap(r => this.af.database.list(`/Users/${user_uid}/dealerships/${dealership_uid}`).remove());
  }

  removeFormDB(f_uid: string) {
    return this.af.database.list(`/Forms/${f_uid}`).remove();
  }

  removeFormST(f_name: string) {
    return firebase.storage().ref(`/Forms/${f_name}`).delete();
  }

  ngOnDestroy() {
  }
}
