import { Injectable } from '@angular/core';

import { AngularFire } from 'angularfire2';

import { Observable } from "rxjs/Rx";

@Injectable()
export class CustomerService {
  _info: any;

  _project_manager: any;
  _dealership: any;

  _products: Observable<any>;

  constructor(private af: AngularFire) {
    this._products = Observable.empty();
  }

  getCustomerInfo(auth) {
      // make sure this user is apart of the customer group
      // use switchMaps to combine the queries into the stream
      console.log("GOT AUTH", auth);
      return Observable.from(this.af.database.object(`/Users/${auth.uid}`))
        .filter(info => info.group == "customer")
        .do( info =>  {
          console.log("INFO", info);
          this._info = info
        })
        .mergeMap( info => {
          // make array in the [key, value] pairs
          let keys = info.dealerships ? Object.keys(info.dealerships) : [];
          console.log('KEYS', keys);
          // for(let k in info.dealerships)
          //   if(info.dealerships[k]) keys.push([k, info.dealerships[k]]);

          if (keys.length === 0) {
            return Observable.empty();
          }

          return Observable.from(keys)
            .filter( key => key !== "*")
            .mergeMap( key => {
              return Observable.from(this.af.database.object(`/Dealerships/${key}`))
                .mergeMap( dealership_info =>
                  Observable.combineLatest(
                    Observable.from(this.af.database.object(`/Users/${dealership_info.project_manager}`)),
                    Observable.from(Object.keys(dealership_info.products || {'*': '*'}))
                      .filter( key => key !== "*")
                      .map(
                        (key) => Observable.from(this.af.database.object(`/Product Building/${key}`))
                      )
                      .combineAll()
                      .map(arr => (arr as any[]).filter( product  => product.$key !== undefined && product.$value !== null)),
                      (pm, products) => {
                        dealership_info.project_manager = pm;
                        dealership_info.products = products;
                        return {dealership_info};
                      })
                )
            })
        }, (info, dealerships) => ({info, dealerships}));
  }

  get info(): any {
    return this._info;
  }

  get project_manager(): any {
    return this._project_manager;
  }

  get products(): Observable<any> {
    return this._products;
  }

  getFormDownloadUrl(f_uid: string) {
    return this.af.database.object(`/Forms/${f_uid}`)
      .map(form => form.downloadUrl);
  }

  ngOnDestroy() {
  }
}
