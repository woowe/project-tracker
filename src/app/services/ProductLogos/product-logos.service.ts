import { Injectable } from '@angular/core';

export const logos = {
  "DealerSocket": {
    alt: "DealerSocket",
    url: "",
    path: "../assets/dealersocket-logo.png"
  },
  "DealerFire": {
    alt: "DealerFire",
    url: "",
    path: "../assets/dealerfire-logo.png"
  },
  "iDMS": {
    alt: "iDMS",
    url: "",
    path: "../assets/idms-logo.png"
  },
  "Inventory+": {
    alt: "Inventory+",
    url: "",
    path: "../assets/inventory+-logo.png"
  },
  "Revenue Radar": {
    alt: "Revenue Radar",
    url: "",
    path: "../assets/revenue-radar-logo.png"
  },
  "We Are Automotive": {
    alt: "We Are Automotive",
    url: "",
    path: "../assets/we-are-automotive-logo.png"
  }
};

@Injectable()
export class ProductLogosService {
  _logos = logos;
  constructor() { }

  get logos() {
    return this._logos;
  }

  getProductLogos() {
    var ret = [];

    for(let logo_name of Object.keys(this._logos)) {
      if( logo_name !== "We Are Automotive")
        ret.push(this._logos[logo_name])
    }
    
    return ret;
  }

  getLogo(logo_name: string) {
    return this._logos[logo_name];
  }
}
