import {
  Component,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate,
  ElementRef,
  ViewChild,
  AfterContentInit
} from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';

import { Observable } from 'rxjs';

import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';
import { UserInfoService } from '../services/UserInfo/user-info.service';
import { fuzzysearch } from '../utils/utils';
import * as ptValidators from '../validators/pt-validators';


@Component({
  selector: 'app-pt-product-template-list',
  templateUrl: './pt-product-template-list.component.html',
  styleUrls: ['./pt-product-template-list.component.scss'],
  animations: [
    trigger('expandState', [
      state('out', style({height: 0})),
      state('in', style({height: "*"})),
      transition("in => out", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
      transition("out => in", animate("0.45s cubic-bezier(.4, 0, .2, 1)"))
    ]),
    trigger('collapserState', [
      state('expand', style({transform: 'rotateZ(180deg)'})),
      state('collapse', style({transform: 'rotateZ(0deg)'})),
      transition("expand <=> collapse", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
    ])
  ]
})
export class PtProductTemplateListComponent implements OnInit, AfterContentInit {
  @ViewChild('query')
  search_input: ElementRef;

  info: any = { selected: {} };

  products$: Observable<any[]>;
  filtered_products$: Observable<any[]>;

  constructor(private pm: ProjectManagerService, private userInfo: UserInfoService,
              private dialog: MdDialog, public snackBar: MdSnackBar) { }

  filterProducts(query: string, products) {
    if(!query) {
      return products;
    }

    var filtered = [];
    for(let product of products) {
      if(fuzzysearch(query, product.name))
        filtered.push(product);
    }
    return filtered;
  }

  onEdit(product) {
    let dialog_ref = this.dialog.open(PtProductTemplateInfoDialog, {
      data: {
        role: 'edit',
        product_model: product
      }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED EDIT", res);
      this.save(product.$key, res);
    })
  }

  onAdd() {
    let dialog_ref = this.dialog.open(PtProductTemplateInfoDialog, {
      data: {
        role: 'add',
        product_model: {
          name: null,
          default_type: null,
          types: []
        }
      }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED ADD", res);
      this.save(null, res);
    })
  }

  onRemove(product_model) {
    let dialog_ref = this.dialog.open(PtRemoveProductTemplateDialog, {
      data: { product_model }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED REMOVE", res);
    })
  }

  save(p_uid: string, product: any) {
    if(!product) { return; }
    if(p_uid) {
      this.snackBar.open(`Updating`, '', { duration: 2000 });
      this.pm.updateProductTemplate(p_uid, product).then(() => {
        this.snackBar.open(`Updated!`, '', { duration: 2000 });
      });
    } else {
      this.snackBar.open(`Adding`, '', { duration: 2000 });
      this.pm.addProductTemplate(product).then(() => {
        this.snackBar.open(`Added!`, '', { duration: 2000 });
      });
    }
  }

  toggleExpanded(key: string) {
    var e = this.info.expanded[key];
    if(e) {
      this.info.expanded[key] = false;
    } else {
      this.info.expanded[key] = true;
    }
  }

  ngOnInit() {
    this.products$ = Observable.from(this.userInfo.auth)
      .first()
      .mergeMap(auth => this.pm.getAllProductTemplates());
  }

  ngAfterContentInit() {
    this.filtered_products$ = Observable.fromEvent(this.search_input.nativeElement, 'input')
      .startWith({ target: { value: '' }})
      .mergeMap( (event: any) => this.products$, (event, products) => ({event, products}) )
      .map( ({event, products}) => event ? this.filterProducts(event.target.value, products) : products );
  }

}

@Component({
  selector: 'pt-remove-product-template-dialog',
  styles: [
    `
    h2, p {
      font-family: 'Roboto', sans-serif;
    }
    `
  ],
  template: `
  <h2 md-dialog-title>Remove Product</h2>
  <md-dialog-content>
    <p> Are you sure? </p>
  </md-dialog-content>
  <md-dialog-actions>
    <button md-button md-dialog-close>No</button>
    <button md-raised-button (click)="onRemove()" color="warn">Yes</button>
  </md-dialog-actions>
  `
})
export class PtRemoveProductTemplateDialog implements OnInit  {

  constructor(public dialogRef: MdDialogRef<PtRemoveProductTemplateDialog>, private pm: ProjectManagerService,
              public snackBar: MdSnackBar) {
  }

  ngOnInit() {
    console.log(this.dialogRef.config.data);
  }

  onRemove() {
    if(this.dialogRef.config.data.product_model) {
      this.snackBar.open(`Removing`, '', { duration: 2000 });
      this.pm.removeProductTemplate(this.dialogRef.config.data.product_model.$key).then(product => {
        this.snackBar.open(`Removed`, '', { duration: 2000 });
      });
      this.dialogRef.close();
    }
  }
}

@Component({
  selector: 'pt-product-info-dialog',
  templateUrl: './pt-product-info-dialog.component.html',
  styleUrls: ['./pt-product-info-dialog.component.scss'],
})
export class PtProductTemplateInfoDialog implements OnInit  {
  _productModelForm: FormGroup;

  milestones: any = {};

  filteredMilestones: Observable<string[]>[] = [];

  constructor(public dialogRef: MdDialogRef<PtProductTemplateInfoDialog>, private pm: ProjectManagerService,
              public snackBar: MdSnackBar, private fb: FormBuilder) {
  }

  onRemove(idx: number) {
    this.snackBar.open(`Deleted ${this._productModelForm.value.types[idx].name}`, '', {
      duration: 1500
    });
    (this._productModelForm.controls['types'] as FormArray).removeAt(idx);
    this.filteredMilestones.splice(idx, 1);
  }

  getKeys(obj: any) {
    return obj ? Object.keys(obj) : null;
  }
  
  displayFn = (m_uid: string) => {
    var milestone = this.milestones[m_uid];
    return milestone ? milestone.name : null;
  }

  filterMilestones(val: string) {
    // console.log('Doing filter...', val);
    if(!val || this.milestones[val]) {
      return [...Object.keys(this.milestones)];
    }

    var filtered = [];
    for(let m_uid of Object.keys(this.milestones)) {
      if(fuzzysearch(val, this.milestones[m_uid].name) || val === this.milestones[m_uid].name)
        filtered.push(m_uid);
    }
    return filtered;

  }

  initForm(product_model?: any) {
    if(!product_model) {
      product_model = {
        name: '',
        default_type: 0,
        types: []
      };
    }
    this._productModelForm = this.fb.group({
      name: [product_model.name || "", Validators.required, ptValidators.productTemplateExsists(this.pm, product_model.name ? [product_model.name] : null)],
      default_type: [product_model.default_type, Validators.required],
      types: this.fb.array([], Validators.minLength(1))
    });

    for(let type of product_model.types) {
      this.pushType(type);
    }
    
    let ta = <FormArray>this._productModelForm.controls['types'];
    for(var i = 0; i < this._productModelForm.value.types.length; ++i) {
      let type = this._productModelForm.value.types[i];
      let fg = <FormGroup>ta.controls[i];
      
      this.filteredMilestones.push(fg.controls['default_milestone_template'].valueChanges
        .startWith(null)
        .map(val => val ? this.filterMilestones(val) : Object.keys(this.milestones)));
    }
  }

  initType(data?: any) {
    let model = {
      name: '',
      default_milestone_template: ''
    };

    if(data) {
      model = data;
    }

    return this.fb.group({
      name: [model.name || "", Validators.required],
      default_milestone_template: [model.default_milestone_template.toString() || "", Validators.required, ptValidators.milestoneTemplateExsists(this.pm, false)]
    });
  }

  pushType(data?: any) {
    let control = <FormArray>this._productModelForm.controls['types'];
    control.push(this.initType(data));

    if(!data) {
      let ta = <FormArray>this._productModelForm.controls['types'];
      var idx = ta.controls.length - 1;
      let fg = <FormGroup>ta.controls[idx];
      this.filteredMilestones.push( fg.controls['default_milestone_template'].valueChanges
          .startWith(null)
          .map(val => val ? this.filterMilestones(val) : Object.keys(this.milestones)) );
      fg.controls['default_milestone_template'].patchValue(this._productModelForm.value.types[idx].default_milestone_template);
    }
  }

  ngOnInit() {
    console.log(this.dialogRef.config.data);
    this.initForm(this.dialogRef.config.data.product_model);
    this.pm.getAllMilestoneTemplates().subscribe( (milestones) => {
      for(let milestone of milestones) {
        this.milestones[milestone.$key] = milestone;
      }
      
      let ta = <FormArray>this._productModelForm.controls['types'];
      for(var i = 0; i < this._productModelForm.value.types.length; ++i) {
        let type = this._productModelForm.value.types[i];
        let fg = <FormGroup>ta.controls[i];
        fg.controls['default_milestone_template'].patchValue(type.default_milestone_template);
      }

      console.log("MILESTONES", this.milestones);
    });
  }
}
