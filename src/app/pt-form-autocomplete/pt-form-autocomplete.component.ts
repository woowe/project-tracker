import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Observable } from 'rxjs';

import { fuzzysearch } from '../utils/utils';

@Component({
  selector: 'pt-form-autocomplete',
  templateUrl: './pt-form-autocomplete.component.html',
  styleUrls: ['./pt-form-autocomplete.component.scss']
})
export class PtFormAutocompleteComponent implements OnInit {
  @Input() placeholder: string = "";
  @Input() forms: any[] = [];
  @Input() fname: string;
  @Input() fgroup: FormGroup;

  filteredForms: Observable<any[]>;

  constructor() { }

  ngOnInit() {
    if(this.fgroup && this.fname) {
      this.filteredForms = this.fgroup.get(this.fname).valueChanges
                            .startWith("")
                            .map(val => this.filterForms(val) );
    }
  }

  displayFn = (f_uid: string) => {
    var form = this.forms.find(f => f.$key === f_uid);
    return form ? form.name : null;
  }

  filterForms(val: string) {
    if(!val || val === "") {
      return this.forms;
    }

    var filtered = [];
    for(let form of this.forms) {
      if(fuzzysearch(val, form.name) || val === form.name)
        filtered.push(form);
    }
    return filtered;
  }

}
