import {
  Component,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/core';

import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';

import { MdDialogRef, MdDialogConfig, MdDialog, MdSnackBar } from '@angular/material';

import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';

import { unsignedGteOneValidator, milestoneTemplateExsists } from '../validators/pt-validators';
import { diffObjArray } from '../utils/utils';

@Component({
  selector: 'app-milestone-editor',
  templateUrl: './milestone-editor.component.html',
  styleUrls: ['./milestone-editor.component.css'],
  animations: [
    trigger('buttonState', [
      state('out', style({transform: 'translate3d(0%, 0%, 0) scale(0)', opacity: 0, zIndex: 1, color: "#fff"})),
      state('in', style({transform: 'translate3d(0%, 0%, 0) scale(1)', opacity: 1, zIndex: 1, color: "#fff"})),
      transition("in => out", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
      transition("out => in", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
    ])
  ]
})
export class MilestoneEditorComponent implements OnInit {
  milestone_model: any;
  add_model: any = {};
  button_state = "in";
  previous_scroll = 0;

  milestone_forms: FormGroup;

  undo_history: any[] = [];
  redo_history: any[] = [];

  forms: any[] = [];

  constructor(public dialogRef: MdDialogRef<MilestoneEditorComponent>,
              private pm: ProjectManagerService,
              public addDialog: MdDialog,
              private fb: FormBuilder,
              public snackBar: MdSnackBar) {
    this.pm.getAllForms().subscribe( forms => {
      this.forms = forms;
    });          
  }

  ngOnInit() {
    if(this.dialogRef.config.data.template && !this.dialogRef.config.data.milestone_model) {
      this.pm.getMilestoneTemplate(this.dialogRef.config.data.template).subscribe( milestone_template => {
        console.log("MILESTONE TEMPALTE:", milestone_template)
        this.initMilestoneModel(milestone_template);
      });
    } else if(this.dialogRef.config.data.milestone_model) {
        this.initMilestoneModel(this.dialogRef.config.data.milestone_model);
    }
  }

  initMilestoneModel(milestone_model) {
    this.milestone_model = milestone_model;
    this.milestone_model.milestones = this._sortMilestones(this.milestone_model.milestones);
    this.milestone_forms = this.fb.group({
      'name': [this.milestone_model.name, Validators.required, milestoneTemplateExsists(this.pm, true, this.milestone_model.name && this.milestone_model.name !== "" ? [this.milestone_model.name] : null)],
      milestones: this.fb.array([])
    });
    for(let milestone of this.milestone_model.milestones) {
      this.addMilestone(milestone)
    }
  }

  addMilestone(milestone: any) {
    let control = <FormArray>this.milestone_forms.controls['milestones'];
    control.push(this.initGroup(milestone));
  }

  initGroup(milestone?: any) {
    var obj = {
      name: '',
      status: '',
      points: '',
      days_differential: '',
      description: '',
      action: '',
      download_link: ''
    };

    if(milestone) {
      obj = milestone;
    }

    return this.fb.group({
      'name': [obj.name, Validators.required],
      'status': [obj.status, Validators.required],
      'points': [obj.points, Validators.compose([Validators.required, unsignedGteOneValidator])],
      'days_differential': [obj.days_differential, Validators.required],
      'description': [obj.description || ""],
      'action': [obj.action || ""],
      'download_link': [obj.download_link || ""]
    });
  }

  _sortMilestones(milestones: any[]): any[] {
    var sum_diff = 0;
    for(var i = 0; i < milestones.length; ++i) {
      sum_diff += Math.abs(milestones[i].days_differential);
    }
    let tmp = milestones.sort((a, b) => {
      a = a.days_differential;
      b = b.days_differential;

      if(Math.sign(a) == -1) {
        a += sum_diff;
      }

      if(Math.sign(b) == -1) {
        b += sum_diff;
      }

      return Math.abs(a) - Math.abs(b);
    });

    let zero_diffs = tmp.filter( t => t.days_differential === 0);

    console.log('SORTED MILESTONES', [...zero_diffs]);

    return [...tmp.slice(zero_diffs.length), ...zero_diffs];
  }

  sortedInsertion(milestone) {
    var milestones = [ ...this.milestone_forms.value.milestones, milestone];
    milestones = this._sortMilestones(milestones);
    var idx = milestones.findIndex(m => m === milestone);
    if(idx && idx >= 0) {
      let control = <FormArray>this.milestone_forms.controls['milestones'];
      control.insert(idx, this.initGroup(milestone));
    }
  }

  applyDiffToForm(old_val: any, new_val: any) {
    var diff = diffObjArray(old_val.milestones, new_val.milestones);
    
    // additions
    for(var i = 0; i < diff.add.length; ++i) {
      this.sortedInsertion(new_val.milestones[diff.add[i]])
    }

    // removals
    var m_a = <FormArray>this.milestone_forms.controls['milestones'];
    for(var i = 0; i < diff.remove.length; ++i) {
      m_a.removeAt(diff.remove[i]);
    }
  }

  onUndo() {
    if (this.undo_history.length > 0) {
      var recent_state = Object.assign({}, this.undo_history.pop());
      this.redo_history.push(JSON.parse(JSON.stringify(this.milestone_forms.value)));
      this.applyDiffToForm(this.milestone_forms.value, recent_state);
      this.snackBar.open('Undo applied', '', {
        duration: 1500
      });
    }
  }

  onRedo() {
    if (this.redo_history.length > 0) {
      var recent_state = Object.assign({}, this.redo_history.pop());
      this.undo_history.push(JSON.parse(JSON.stringify(this.milestone_forms.value)));
      this.applyDiffToForm(this.milestone_forms.value, recent_state);
      this.snackBar.open('Redo applied', '', {
        duration: 1500
      });
    }
  }

  onRemove(idx: number) {
    this.undo_history.push(JSON.parse(JSON.stringify(this.milestone_forms.value)));
    this.snackBar.open(`Deleted ${this.milestone_forms.value.milestones[idx].name}`, '', {
      duration: 1500
    });
    (this.milestone_forms.controls['milestones'] as FormArray).removeAt(idx);
  }

  onSave() {
    let ret = this.milestone_forms.value;

    // for(let milestone of ret.milestones) {
    //   if(milestone.download_link !== '' && milestone.download_link) {
    //     let form = this.forms.find(f => f.$key === milestone.download_link);
    //     milestone.download_link = form ? form.downloadUrl : '';
    //   }
    // }

    console.log('RET', ret);

    this.dialogRef.close(ret);
  }

  onAddFabClicked(e) {
    let dialog_ref = this.addDialog.open(AddMilestoneDialog);
    dialog_ref.afterClosed().subscribe(res => {
      // console.log('ADD DIALOG RES:', res);
      if(res) {
        this.addToForm(res);
      }
    });
  }

  addToForm(milestone) {
    this.undo_history.push(JSON.parse(JSON.stringify(this.milestone_forms.value)));
    if(this.milestone_forms.value.milestones.length > 1) {
      this.sortedInsertion(milestone);
    } else {
      let control = <FormArray>this.milestone_forms.controls['milestones'];
      control.insert(0, this.initGroup(milestone));
    }
    this.snackBar.open(`Added ${milestone.name}`, '', {
      duration: 1500
    });
  }

  onListScroll(e) {
    if(e.target.scrollTop - this.previous_scroll > 0 && this.button_state !== 'out') {
      this.button_state = 'out';
    } else if(e.target.scrollTop - this.previous_scroll < 0 && this.button_state !== 'in') {
      this.button_state = 'in';
    }
    this.previous_scroll = e.target.scrollTop;
  }

}

@Component({
  selector: 'add-milestone-dialog',
  templateUrl: './add-milestone-dialog.html',
  styleUrls: [ './add-milestone-dialog.scss' ]
})
export class AddMilestoneDialog {
  add_form;
  constructor(public dialogRef: MdDialogRef<AddMilestoneDialog>, private fb: FormBuilder) {
    this.add_form = this.fb.group({
      'name': ['', Validators.required],
      'status': ['In Progress', Validators.required],
      'points': [1, Validators.compose([Validators.required, unsignedGteOneValidator])],
      'days_differential': [0, Validators.required],
      'description': [''],
      'action': [''],
      'download_link': [''],
    });
  }

  onClear() {
    this.add_form.reset();
  }
}