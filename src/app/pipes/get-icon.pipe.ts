import { Pipe, PipeTransform } from '@angular/core';

import { getIcon } from '../utils/utils';

@Pipe({
  name: 'getIcon'
})
export class GetIconPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    return value ? getIcon(value): null;
  }

}
