import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getDateMS'
})
export class GetDateMSPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value ? Date.parse(value) : null;
  }

}
