import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getMilestoneStatusIcon'
})
export class GetMilestoneStatusIconPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch(value) {
      case 'In Progress':
        return 'loop';
      case 'Needs Attention':
        return 'error';
      case 'Complete':
        return 'done';
      default:
        break;
    }
    return '';
  }

}
