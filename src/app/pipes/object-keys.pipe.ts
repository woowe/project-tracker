import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectKeys'
})
export class ObjectKeysPipe implements PipeTransform {

  transform(value: Object): any {
    console.log("KEYS", value);
    return value ? Object.keys(value) : null;
  }

}
