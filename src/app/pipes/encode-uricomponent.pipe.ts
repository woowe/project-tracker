import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'encodeURIComponent'
})
export class EncodeURIComponentPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value ? encodeURIComponent(value) : null;
  }

}
