import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getMilestoneStatusClass'
})
export class GetMilestoneStatusClassPipe implements PipeTransform {

  transform(status: string): any {
    switch(status) {
      case 'In Progress':
        return 'in-progress';
      case 'Needs Attention':
        return 'needs-attention'
      case 'Complete':
        return 'complete'
      default:
        break;
    }
    
    return '';
  }

}
