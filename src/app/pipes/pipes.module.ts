import { NgModule } from '@angular/core';

import { IsProductPipe } from './is_product.pipe';
import { PhoneFormatPipe } from './phone_format.pipe';
import { SafeUrlPipe } from './safe_url.pipe';
import { ToPathPipe } from './to_path.pipe';
import { DescribeArcPipe } from './describe-arc.pipe';
import { GetIconPipe } from './get-icon.pipe';
import { GetDateMSPipe } from './get-date-ms.pipe';
import { EncodeURIComponentPipe } from './encode-uricomponent.pipe';
import { GetMilestoneStatusIconPipe } from './get-milestone-status-icon.pipe';
import { GetMilestoneETADatePipe } from './get-milestone-etadate.pipe';
import { GetMilestoneStatusClassPipe } from './get-milestone-status-class.pipe';
import { ObjectKeysPipe } from './object-keys.pipe';
import { ObjectEqPipe } from './object-eq.pipe';

export const PIPES_DECLARATIONS = [
    IsProductPipe,
    PhoneFormatPipe,
    SafeUrlPipe,
    ToPathPipe,
    DescribeArcPipe,
    GetIconPipe,
    GetDateMSPipe,
    EncodeURIComponentPipe,
    GetMilestoneStatusIconPipe,
    GetMilestoneETADatePipe,
    GetMilestoneStatusClassPipe,
    ObjectKeysPipe,
    ObjectEqPipe
]

@NgModule({
    declarations: [
        PIPES_DECLARATIONS,
    ],
    exports: [
        PIPES_DECLARATIONS
    ]
})
export class PipesModule {} 