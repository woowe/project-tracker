import { Pipe, PipeTransform } from '@angular/core';

import { getDayDiff } from '../utils/utils';

@Pipe({
  name: 'getMilestoneETADate'
})
export class GetMilestoneETADatePipe implements PipeTransform {

  transform(milestone: any, product: any): number {
    if(product && milestone) {
      let start_ms = Date.parse(product.started), end_ms = Date.parse(product.activation);
      if(start_ms < end_ms) {
        // console.log("DATE RIGHT!", start_ms, end_ms, milestone.days_differential);
        return getDayDiff(start_ms, end_ms, milestone.days_differential);
      } else {
        console.log('DATE WRONG', product.started, product.activation, milestone.days_differential);
      }
    }
    
    return null;
  }

}
