import { Pipe, PipeTransform } from '@angular/core';

import { objectEq } from '../utils/utils';

@Pipe({
  name: 'objectEq'
})
export class ObjectEqPipe implements PipeTransform {

  transform(o1: Object, o2: Object): any {
    console.log('OBJECT EQ', o1, o2, objectEq(o1, o2));
    return o1 && o2 ? objectEq(o1, o2) : null;
  }

}
