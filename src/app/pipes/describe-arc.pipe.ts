import { Pipe, PipeTransform } from '@angular/core';


import { describeArc } from '../utils/utils';

// getArc(50, 50, 43.5, 0, ( (product.completion_info)?.percent_complete || 0 ) / 100 * 360)

interface Arc {
  x: number,
  y: number,
  r: number
}

@Pipe({
  name: 'describeArc'
})
export class DescribeArcPipe implements PipeTransform {

  transform(value: number, args: Arc): string {
    // console.log("ARC PIPE", value, args);
    return describeArc(args.x, args.y, args.r, 0, value);
  }

}
