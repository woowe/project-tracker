import {
  Component,
  OnInit,
  // ElementRef,
  ChangeDetectorRef,
  ApplicationRef,
  HostListener,
  // Query,
  // QueryList
} from '@angular/core';

import { Router } from '@angular/router';
import { ProductLogosService } from '../services/ProductLogos/product-logos.service';
import { UserInfoService } from '../services/UserInfo/user-info.service';
import { CustomerService } from '../services/Customer/customer.service';

import { Observable } from "rxjs/Rx";

import { calculateMilestoneCompletion, objectEq } from '../utils/utils';

@Component({
  selector: 'pt-customer',
  templateUrl: './pt-customer.component.html',
  styleUrls: ['./pt-customer.component.scss'],
})
export class PtCustomerComponent implements OnInit {
  user_info: any;
  dealerships: any = {};
  selected_uid: string = null;
  local_storage_namespace: string;

  constructor(private productLogos: ProductLogosService, private router: Router, private ref: ChangeDetectorRef, private appRef: ApplicationRef,
              private userInfo: UserInfoService, private customer: CustomerService) {}
  
  objectEq(o1, o2) {
    return objectEq(o1, o2);
  }

  getKeys(obj: any) {
    return Object.keys(obj);
  }

  getNumProducts() {
    var num_products = 0;
    for(let uid of Object.keys(this.dealerships)) {
      let dealership = this.dealerships[uid];
      for(let product of dealership.products) {
        if(product.completion_info) {
          if(product.completion_info.percent_complete < 100) {
            num_products++;
          }
        }
      }
    }

    return num_products;
  }

  isPrimaryContact(dealership) {
    for(let uid of Object.keys(dealership.users)) {
      let role = dealership.users[uid];
      if(role === "primary" && uid === this.user_info.$key) {
        return true;
      }
    }
    return false;
  }

  updateSelectedUid(uid: string) {
    this.selected_uid = uid;

    // this.ref.detectChanges();
    // this.appRef.tick();
  }

  ngOnInit() {

    Observable.from(this.userInfo.auth)
      .filter(auth => auth != null)
      .first()
      .mergeMap(auth => this.customer.getCustomerInfo(auth), (auth, info) => ({auth, info}))
      .subscribe(res => {
        this.user_info = res.info.info;
        this.local_storage_namespace = this.user_info.$key + ":PCC";

        if( res.info .dealerships ) {
          var dealership = (<any>res.info.dealerships).dealership_info;
          var uid = dealership.$key;
          
          dealership.user_is_primary = this.isPrimaryContact(dealership);
          this.dealerships[uid] = dealership;

          var tmp = this.productLogos.getProductLogos().map(function (v) { return { logo_info: v } });

          for(let product of this.dealerships[uid].products) {
            product.completion_info = calculateMilestoneCompletion(product, product.milestone_model);
            // product.completion_info.percent_complete = 50;
            tmp[product.type] = Object.assign(tmp[product.type], product);
          }

          this.dealerships[uid].products = tmp;

          let storage_obj = localStorage.getItem(`${this.local_storage_namespace}`);

          if(storage_obj) {
            storage_obj = JSON.parse(storage_obj);

            for(let feild of Object.keys(storage_obj)) {
              if(storage_obj[feild])
                this[feild] = storage_obj[feild];
            }
          }

          if(!this.selected_uid || this.selected_uid === uid) {
            this.selected_uid = uid;
          }
        }
        // console.log(this.dealerships);
      },
      (error) => {
        console.error(error);
      },
      (/*complete*/) => {
        console.log('Completed');
        if(this.userInfo._info)
          this.user_info = this.userInfo._info;
      });
  }

  @HostListener('window:unload', ['$event'])
  saveState() {
    let storage_obj: any = {
      selected_uid: this.selected_uid
    };

    localStorage.setItem(`${this.local_storage_namespace}`, JSON.stringify(storage_obj));
  }

  ngOnDestroy() {
    this.saveState();
  }
}
