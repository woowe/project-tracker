import { Component, OnInit, AfterViewChecked, trigger, state, style, transition, animate } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { ProductLogosService } from '../services/ProductLogos/product-logos.service';
import { UserInfoService } from '../services/UserInfo/user-info.service';
import { CustomerService } from '../services/Customer/customer.service';

import { Observable } from "rxjs/Rx";

import { getDayDiff, getIcon, calculateMilestoneCompletion } from '../utils/utils';

@Component({
  selector: 'app-milestone-tracker',
  templateUrl: './milestone-tracker.component.html',
  styleUrls: ['./milestone-tracker.component.css']
})
export class MilestoneTrackerComponent implements OnInit, AfterViewChecked {
  products;
  dealerships: any = {};
  selected_uid: string;
  tab: number = 0;

  private sub: any;

  constructor(private productLogos: ProductLogosService, private route: ActivatedRoute,
              private userInfo: UserInfoService, private customer: CustomerService) {}

  _sortMilestones(milestones: any[]): any[] {
    var sum_diff = 0;
    for(var i = 0; i < milestones.length; ++i) {
      sum_diff += Math.abs(milestones[i].days_differential);
    }
    let tmp = milestones.sort((a, b) => {
      a = a.days_differential;
      b = b.days_differential;

      if(Math.sign(a) == -1) {
        a += sum_diff;
      }

      if(Math.sign(b) == -1) {
        b += sum_diff;
      }

      return Math.abs(a) - Math.abs(b);
    });

    let zero_diffs = tmp.filter( t => t.days_differential === 0);

    return [...tmp.slice(zero_diffs.length), ...zero_diffs];
  }

  ngOnInit() {
    this.products = this.productLogos.getProductLogos().map(function (v) { return { logo_info: v } });

    Observable.from(this.userInfo.auth)
      .filter(auth => auth != null)
      .first()
      .mergeMap(auth => this.route.params, (auth, params) => ({auth, params}))
      .mergeMap(({auth, params}) => this.customer.getCustomerInfo(auth), ({auth, params}, info) => ({auth, params, info}))
      .subscribe(res => {
        this.selected_uid = res.params['uid'];
        this.tab = +res.params['tab'];

        if(res.info.dealerships) {
          var dealership = (<any>res.info.dealerships).dealership_info;
          var uid = dealership.$key;
          
          this.dealerships[uid] = dealership;

          var tmp = this.productLogos.getProductLogos().map(function (v) { return { logo_info: v } });

          for(let product of this.dealerships[uid].products) {
            product.milestone_model.milestones = this._sortMilestones(product.milestone_model.milestones);
            product.completion_info = calculateMilestoneCompletion(product, product.milestone_model);

            for(let milestone of product.milestone_model.milestones) {
              if(milestone.download_link) {
                milestone.downloadUrl$ = this.customer.getFormDownloadUrl(milestone.download_link);
              }
            }

            tmp[product.type] = Object.assign(tmp[product.type], product);
          }

          this.dealerships[uid].products = tmp;
        }

        console.log(this.dealerships, res.params);
      });

  }

  ngAfterViewChecked() {
  }

  ngOnDestroy() {
  }

}
