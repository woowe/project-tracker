import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import * as ptValidators from '../validators/pt-validators';

@Component({
  selector: 'pt-milestone-info',
  templateUrl: './pt-milestone-info.component.html',
  styleUrls: ['./pt-milestone-info.component.scss']
})
export class PtMilestoneInfoComponent implements OnInit {
  _milestoneFormGroup: FormGroup;
  _forms: any[] = [];

  action_applied = false;

  constructor() {}

  ngOnInit() {
  }

  @Input()
  get forms() {
    return this._forms;
  }

  set forms(forms: any[]) {
    this._forms = forms;

    if(this._milestoneFormGroup && this.forms.length > 0) {
      this.onActionChanged();
    }
  }

  @Input()
  get milestoneFormGroup() {
    return this._milestoneFormGroup;
  }

  onActionChanged() {
    let dl = this._milestoneFormGroup.controls['download_link'];
    // console.log('ACTION VALUE', this._milestoneFormGroup.value.action, this._forms);
    if(this._milestoneFormGroup.value.action === 'Form Download') {
      dl.enable();
      dl.setValidators(Validators.compose([Validators.required, ptValidators.formValidator(this._forms)]));
      dl.updateValueAndValidity();
    } else {
      dl.patchValue('');
      dl.setValidators(Validators.nullValidator);
      dl.updateValueAndValidity();
      dl.disable();
    }
  }

  set milestoneFormGroup(group: FormGroup) {
    this._milestoneFormGroup = group;

    if(this._milestoneFormGroup && this.forms.length > 0) {
      this.onActionChanged();
    }
  }

}
