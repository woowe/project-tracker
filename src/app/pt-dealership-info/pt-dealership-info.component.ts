import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  ViewChild,
  ElementRef,
  trigger,
  state,
  style,
  transition,
  animate,
  ChangeDetectorRef,
  NgZone
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs';

@Component({
  selector: 'pt-dealership-info',
  templateUrl: './pt-dealership-info.component.html',
  styleUrls: ['./pt-dealership-info.component.scss'],
  animations: [
    trigger('expandState', [
      state('out', style({height: 0})),
      state('in', style({height: "*"})),
      transition("in <=> out", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
    ]),
    trigger('collapserState', [
      state('in', style({transform: 'rotateZ(180deg)'})),
      state('out', style({transform: 'rotateZ(0deg)'})),
      transition("in <=> out", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
    ])
  ]
})
export class PtDealershipInfoComponent implements OnInit {
  @Output() change = new EventEmitter();

  dealership_form: FormGroup;

  submit_val = false;

  map: any;
  marker: any;

  preview_map_state: string = 'out';

  @ViewChild('location')
  locationRef: ElementRef;

  @ViewChild('map')
  mapRef: ElementRef;
  autocomplete = null;

  constructor(private fb: FormBuilder, private ref: ChangeDetectorRef, private _ngZone: NgZone) {
  }

  ngOnInit() {
    this.map = new google.maps.Map(this.mapRef.nativeElement, {
      center: {lat: -34.397, lng: 150.644},
      zoom: 8
    });

    this.marker = new google.maps.Marker();

    this.autocomplete = new google.maps.places.Autocomplete(this.locationRef.nativeElement, {
      types: ['geocode']
    });

    // autocomplete.addListener("change", setPlace);
    this.autocomplete.addListener("place_changed", () => {
      //get the place result
      let place: google.maps.places.PlaceResult = this.autocomplete.getPlace();

      if(!place) {
        return;
      }

      //verify result
      if (place.geometry === undefined || place.geometry === null) {
        return;
      }

      // console.log(place);

      this.dealership_form.get('location').patchValue(place.formatted_address);

      if(this.marker) {
        this.marker.setMap(null);
        let loc = {
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng(),
        };

        this.marker = new google.maps.Marker({
          position: loc,
          map: this.map
        });

        this.map.setCenter(loc);
      }
    });
  }

  toggleExpanded() {
    this.preview_map_state = this.preview_map_state === 'in' ? 'out' : 'in';
  }

  onValueChanged(data?: any): void {
    if (!this.dealership_form) { return; }
    const form = this.dealership_form;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  @Input()
  get dealershipFormGroup() {
    return this.dealership_form;
  }

  set dealershipFormGroup(group: FormGroup) {
    console.log("GOT FORM GROUP", group.value);

    this.dealership_form = group;

    this.dealership_form.valueChanges
      .subscribe( data => {
        this.change.emit(this.dealership_form.value);
        this.onValueChanged(data)
      });

    this.dealership_form.get('location').statusChanges
      .subscribe( data => this.onValueChanged(data) );

    this.onValueChanged();
  }

  formErrors = {
    'name': '',
    'location': '',
  };
  validationMessages = {
    'name': {
      'required': 'Name is required.'
    },
    'location': {
      'required': 'Location is required.'
    }
  };

}
