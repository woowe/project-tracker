/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PtProductCompleteChartComponent } from './pt-product-complete-chart.component';

describe('PtProductCompleteChartComponent', () => {
  let component: PtProductCompleteChartComponent;
  let fixture: ComponentFixture<PtProductCompleteChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtProductCompleteChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtProductCompleteChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
