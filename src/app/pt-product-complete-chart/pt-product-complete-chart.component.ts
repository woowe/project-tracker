import {
  Component,
  OnInit,
  Input,
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/core';

import { Observable } from 'rxjs';

import { describeArc, calculateMilestoneCompletion, getIcon } from '../utils/utils';

@Component({
  selector: 'pt-product-complete-chart',
  templateUrl: './pt-product-complete-chart.component.html',
  styleUrls: ['./pt-product-complete-chart.component.scss'],
  animations: [
    trigger('arcLoad', [
      state('start', style({ strokeDashoffset: -286, strokeDasharray: 286, stroke: 'white' })),
      state('end-bad', style({ strokeDashoffset: 0, strokeDasharray: 286, stroke: '#EF5350' })),
      state('end-good', style({ strokeDashoffset: 0, strokeDasharray: 286, stroke: '#66BB6A' })),
      transition("* => *", animate(`750ms ease`))
    ])
  ]
})
export class PtProductCompleteChartComponent implements OnInit {
  @Input() products: any[] = [];

  colors = {
    'On Schedule': '#66BB6A',
    'Needs Attention': '#EF5350'
  };

  constructor() {
    // this.data 
  }

  yieldByInterval(items, time) {
    return Observable.from(items).zip(
      Observable.interval(time),
      function(item, index) { return item; }
    );
  }

  ngOnInit() {
    for(let product of this.products) {
      if(product.milestone_model)
        product.completion_info = calculateMilestoneCompletion(product, product.milestone_model);
    }
  }
}
