import {
  Component,
  OnInit,
  AfterViewInit,
  AfterViewChecked,
  Input,
  trigger,
  state,
  style,
  transition,
  animate,
  HostListener,
  ChangeDetectorRef
} from '@angular/core';

import { Router } from '@angular/router';
import { Observable } from "rxjs/Rx";

import { ProductLogosService } from '../services/ProductLogos/product-logos.service';
import { describeArc, getIcon, objectEq } from '../utils/utils';

@Component({
  selector: 'product-selection',
  templateUrl: './product-selection.component.html',
  styleUrls: ['./product-selection.component.css'],
  animations: [
    trigger('arcLoad', [
      state('start', style({ strokeDashoffset: -286, strokeDasharray: 286, stroke: 'white' })),
      state('end-bad', style({ strokeDashoffset: 0, strokeDasharray: 286, stroke: '#EF5350' })),
      state('end-good', style({ strokeDashoffset: 0, strokeDasharray: 286, stroke: '#66BB6A' })),
      transition("* => *", animate(`750ms ease`))
    ]),
    trigger('productState', [
      state('start', style({ color: 'white' })),
      state('end-bad', style({ color: '#EF5350' })),
      state('end-good', style({ color: '#66BB6A' })),
      transition("* => *", animate(`750ms ease`))
    ])
  ]
})
export class ProductSelectionComponent implements OnInit, AfterViewChecked, AfterViewInit {

  demo_urls: string[] = [
    'http://dealersocket.com/independent-CRM',
    'http://dealersocket.com/independent-websites',
    'http://dealersocket.com/independent-DMS',
    'http://dealersocket.com/inventory',
    'http://dealersocket.com/equity-mining'
  ];

  _dealerships  = {};
  _products: any[] = [];
  _projectManager: any = {};
  _selectedUid: string;

  @Input()
  get selectedUid() {
    return this._selectedUid; 
  }

  set selectedUid(selectedUid: string) {
    this._selectedUid = selectedUid;

    if(this._selectedUid && this._dealerships) {
      this.reset();
    }
  }

  @Input()
  get dealerships() {
    return this._dealerships;
  }

  set dealerships(dealerships: any) {
    this._dealerships = dealerships;

    if(this._selectedUid && this._dealerships) {
      this.reset();
    }
  }

  arcs_applied = false;
  prev_scroll = 0;

  constructor(private router: Router, private productLogos: ProductLogosService,
              private ref: ChangeDetectorRef) {}
  
  getDateMs(date: string) {
    // console.log(date);
    if(date)
      return Date.parse(date);
    return null;
  }

  getKeys(obj: any) {
    return Object.keys(obj);
  }

  getArc(x, y, radius, startAngle, endAngle) {
    return describeArc(x, y, radius, startAngle, endAngle);
  }

  getIcon(alt: string) {
    return getIcon(alt);
  }

  setArcState(product: any) {
    if(product.completion_info.status !== "On Schedule") {
      product.arc_state = "end-bad";
      product.product_state = "end-bad";
    } else {
      product.arc_state = "end-good";
      product.product_state = "end-good";
    }
  }

  reset() {
    // console.log('calling reset', this._selectedUid, Object.keys(this._dealerships), this._dealerships[this._selectedUid]);
    this._products = [];
    this._projectManager = {};

    if(this._dealerships[this._selectedUid]){
      console.log('doing reset');
      this._products = this._dealerships[this._selectedUid].products;
      this._projectManager = this._dealerships[this._selectedUid].project_manager;
      this.arcs_applied = false;
    }

    // this.applyArcStates();
  }

  applyArcStates() {
    let ob = Observable.from(this._products)
      .filter((p: any) => p.completion_info)
      .delay(550)
      .subscribe((p) => {
        this.setArcState(p);
      });

    this.arcs_applied = true;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    let selectionContainer = document.querySelector('.product-selection-container');

    if(selectionContainer && window.innerWidth < 1281) {
      (selectionContainer as any).style.backgroundImage = "";
    } else if (window.innerWidth > 1280) {
      // console.log('setting background');
      (selectionContainer as any).style.backgroundImage = "url(../assets/product-selection-background.png)";
    }
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event?: any) {
    if(!event) {
      event = {};
      event.target = {scrollTop: 0};
    }

    let scrollTop = event.target.scrollTop;

    let product_elements = document.querySelectorAll('.product-container');
    let viewport_middle = { x: window.innerWidth / 2, y: window.innerHeight / 2 };
    let product_boxes = [];

    for(var i = 0; i < product_elements.length; ++i) {
      let box = product_elements.item(i).getBoundingClientRect();
      product_boxes.push({
        el: product_elements.item(i),
        cx: box.left + box.width / 2,
        cy: box.top + box.height / 2 + 75 * Math.sign(scrollTop - this.prev_scroll)
      });
    }

    var dist = (x1, y1, x2, y2) => {
      return Math.sqrt( Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    };

    var closest = product_boxes[0];

    if(closest && scrollTop > 0) {
      closest.dist = dist(closest.cx, closest.cy, viewport_middle.x, viewport_middle.y);
      for(let box of product_boxes) {
        var d = dist(box.cx, box.cy, viewport_middle.x, viewport_middle.y);
        if(d <= closest.dist) {
          closest = box;
          closest.dist = d;
        }
        box.el.classList.remove('hover');
      }

      closest.el.classList.add('hover');
    }

    this.prev_scroll = scrollTop;
  }

  ngAfterViewChecked() {
    if(this._products.length === 0 || objectEq(this._projectManager, {})) {
      this.reset();
      this.ref.detectChanges();
    }

    if(!this.arcs_applied && this._products) {
      for(let product of this._products) {
        product = Object.assign(product, { arc_state: "start", product_state: "start" });
      }

      this.ref.detectChanges();

      this.applyArcStates();
      this.onScroll();
    }
  }

  ngOnInit() {
    if(this._products.length === 0 || objectEq(this._projectManager, {})) {
      this.reset();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.onResize();
    }, 550);

    if(window.innerWidth < 500) {
      document.querySelector('.mat-sidenav-content').addEventListener('scroll', this.onScroll);
      this.onScroll();
    }
  }

  ngOnDestroy() {
  }
}
