import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule, MdDialogModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';

import { CarouselModule } from 'primeng/primeng';
import { Md2Module } from 'md2';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

import { ProductLogosService } from './services/ProductLogos/product-logos.service';
import { CustomerService } from './services/Customer/customer.service';
import { ProjectManagerService } from './services/ProjectManager/project-manager.service';
import { AuthGuardService } from './services/AuthGuards/auth-guard.service';
import { UserInfoService } from './services/UserInfo/user-info.service';
import { PmDashboardService } from './services/PmDashboard/pm-dashboard.service';

import 'hammerjs';
import { ProductSelectionComponent } from './product-selection/product-selection.component';

import { PipesModule } from './pipes/pipes.module';
import { DirectivesModule } from './directives/directives.module';

import { MilestoneTrackerComponent } from './milestone-tracker/milestone-tracker.component';
import { ProjectManagerDashboardComponent, AddDealershipDialog } from './project-manager-dashboard/project-manager-dashboard.component';
import { MilestoneEditorComponent, AddMilestoneDialog } from './milestone-editor/milestone-editor.component';
import { FindPeopleComponent } from './find-people/find-people.component';
import { UserSliderComponent } from './user-slider/user-slider.component';
import { PtListComponent } from './pt-list/pt-list.component';
import { PtChipListComponent } from './pt-chip-list/pt-chip-list.component';
import { PtFormContainerComponent } from './pt-form-container/pt-form-container.component';

import { environment } from '../environments/environment';
import { PtUserSearchComponent } from './pt-user-search/pt-user-search.component';
import { PtUserInfoComponent } from './pt-user-info/pt-user-info.component';
import { PtDealershipInfoComponent } from './pt-dealership-info/pt-dealership-info.component';
import { PtMilestoneEditorComponent } from './pt-milestone-editor/pt-milestone-editor.component';
import { PtProductSelectorComponent } from './pt-product-selector/pt-product-selector.component';
import { PtMilestoneInfoComponent } from './pt-milestone-info/pt-milestone-info.component';
import { PtSvgMorphComponent } from './pt-svg-morph/pt-svg-morph.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PtDashComponent, PtDashMenuComponent } from './pt-dash/pt-dash.component';
import { PtCustomerComponent } from './pt-customer/pt-customer.component';
import { PtProductCompleteChartComponent } from './pt-product-complete-chart/pt-product-complete-chart.component';
import { PtPmAccountComponent } from './pt-pm-account/pt-pm-account.component';
import { PtDealeshipOverviewComponent } from './pt-dealeship-overview/pt-dealeship-overview.component';
import { PtUserListComponent, PtUserEditInfoDialog, PtRemoveCustomerDialog } from './pt-user-list/pt-user-list.component';
import { PtMilestoneTemplateListComponent, PtRemoveMilestoneTemplateDialog } from './pt-milestone-template-list/pt-milestone-template-list.component';
import { PtProductTemplateListComponent, PtProductTemplateInfoDialog, PtRemoveProductTemplateDialog } from './pt-product-template-list/pt-product-template-list.component';
import { PtUploadPdfComponent } from './pt-upload-pdf/pt-upload-pdf.component';
import { PtFormListComponent, PtFormRemoveDialogComponent } from './pt-form-list/pt-form-list.component';
import { PtLabelToClipboardComponent } from './pt-label-to-clipboard/pt-label-to-clipboard.component';
import { PtFormAutocompleteComponent } from './pt-form-autocomplete/pt-form-autocomplete.component';
import { PtLogoDirective } from './directives/pt-logo.directive';

declare var google: any;

console.log(environment.firebaseConfig.authDomain === "project-tracker-staging.firebaseapp.com" ? 'Development Environment!' : 'Production Environment!');

const myFirebaseConfig = environment.firebaseConfig;

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};

const appRoutes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'customer', redirectTo: 'customer/product-selection', pathMatch: 'full' },
  { path: 'customer/product-selection', component: PtCustomerComponent, canActivate: [AuthGuardService] },
  { path: 'customer/milestone-tracker/:uid/:tab', component: MilestoneTrackerComponent, canActivate: [AuthGuardService] },
  { path: 'project-manager', component: ProjectManagerDashboardComponent, canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: 'dealership-overview', pathMatch: 'full' },
      { path: 'dealership-overview', component: PtDealeshipOverviewComponent },
      { path: 'account', component: PtPmAccountComponent },
      { path: 'user-list', component: PtUserListComponent },
      { path: 'milestone-template-list', component: PtMilestoneTemplateListComponent },
      { path: 'product-template-list', component: PtProductTemplateListComponent },
      { path: 'form-list', component: PtFormListComponent },
    ] },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProductSelectionComponent,
    MilestoneTrackerComponent,
    ProjectManagerDashboardComponent,
    AddDealershipDialog,
    FindPeopleComponent,
    UserSliderComponent,
    PtListComponent,
    PtChipListComponent,
    PtFormContainerComponent,
    PtUserSearchComponent,
    PtUserInfoComponent,
    PtDealershipInfoComponent,
    PtMilestoneEditorComponent,
    PtProductSelectorComponent,
    MilestoneEditorComponent,
    AddMilestoneDialog,
    PtMilestoneInfoComponent,
    PtSvgMorphComponent,
    NotFoundComponent,
    PtDashComponent,
    PtDashMenuComponent,
    PtCustomerComponent,
    PtProductCompleteChartComponent,
    PtPmAccountComponent,
    PtDealeshipOverviewComponent,
    PtUserListComponent,
    PtUserEditInfoDialog,
    PtRemoveCustomerDialog,
    PtMilestoneTemplateListComponent,
    PtRemoveMilestoneTemplateDialog,
    PtProductTemplateListComponent,
    PtProductTemplateInfoDialog,
    PtRemoveProductTemplateDialog,
    PtUploadPdfComponent,
    PtFormListComponent,
    PtFormRemoveDialogComponent,
    PtLabelToClipboardComponent,
    PtFormAutocompleteComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    PipesModule,
    DirectivesModule,
    MaterialModule,
    MdDialogModule,
    AngularFireModule.initializeApp(myFirebaseConfig, myFirebaseAuthConfig),
    CarouselModule,
    ReactiveFormsModule,
    Md2Module.forRoot()
  ],
  entryComponents: [
    AddDealershipDialog,
    MilestoneEditorComponent,
    AddMilestoneDialog,
    PtUserEditInfoDialog,
    PtRemoveCustomerDialog,
    PtRemoveMilestoneTemplateDialog,
    PtProductTemplateInfoDialog,
    PtRemoveProductTemplateDialog,
    PtFormRemoveDialogComponent
  ],
  providers: [
    ProductLogosService,
    UserInfoService,
    AuthGuardService,
    CustomerService,
    ProjectManagerService,
    PmDashboardService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
