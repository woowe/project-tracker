import {
  Component,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate,
  ViewChild,
  AfterContentInit,
  ElementRef
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdDialog, MdDialogRef, MdSnackBar, MdInputDirective } from '@angular/material';

import { Observable } from 'rxjs';

import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';
import { UserInfoService } from '../services/UserInfo/user-info.service';
import { fuzzysearch } from '../utils/utils';
import * as ptValidators from '../validators/pt-validators';

@Component({
  selector: 'pt-user-list',
  templateUrl: './pt-user-list.component.html',
  styleUrls: ['./pt-user-list.component.scss'],
  animations: [
    trigger('expandState', [
      state('out', style({height: 0})),
      state('in', style({height: "*"})),
      transition("in => out", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
      transition("out => in", animate("0.45s cubic-bezier(.4, 0, .2, 1)"))
    ]),
    trigger('collapserState', [
      state('expand', style({transform: 'rotateZ(180deg)'})),
      state('collapse', style({transform: 'rotateZ(0deg)'})),
      transition("expand <=> collapse", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
    ])
  ]
})
export class PtUserListComponent implements OnInit, AfterContentInit {
  @ViewChild('query')
  search_input: ElementRef;

  info: any = { selected: {}, expanded: {} };

  activePage;

  users$: Observable<any[]>;
  filteredUsers$: Observable<any[]>;

  constructor(private pm: ProjectManagerService, private userInfo: UserInfoService,
              private dialog: MdDialog) { }

  filterUsers(query: string, users) {
    if(!query) {
      return users;
    }

    var filtered = [];
    for(let user of users) {
      if(fuzzysearch(query, user.name))
        filtered.push(user);
    }
    return filtered;
  }

  onEdit(user) {
    let dialog_ref = this.dialog.open(PtUserEditInfoDialog, {
      data: {
        role: 'edit',
        user
      }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED EDIT", res);
    })
  }

  onAddUser() {
    let dialog_ref = this.dialog.open(PtUserEditInfoDialog, {
      data: {
        role: 'add',
        user: null
      }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED ADD", res);
    })
  }

  onRemoveUser(user) {
    let dialog_ref = this.dialog.open(PtRemoveCustomerDialog, {
      data: { user }
    });

    dialog_ref.afterClosed().subscribe((res: any) => {
      console.log("AFTER CLOSED REMOVE", res);
    })
  }

  toggleExpanded(key: string) {
    var e = this.info.expanded[key];
    if(e) {
      this.info.expanded[key] = false;
    } else {
      this.info.expanded[key] = true;
    }
  }

  returnSelectedModel(key: string) {
    if(!this.info.selected[key]) {
      this.info.selected[key] = true;
    }
    return this.info.selected[key];
  }

  ngOnInit() {
    this.users$ = Observable.from(this.userInfo.auth)
      .first()
      .mergeMap(auth => this.pm.getAllUsers());
  }

  ngAfterContentInit() {
    this.filteredUsers$ = Observable.fromEvent(this.search_input.nativeElement, 'input')
      .startWith({ target: { value: '' }})
      .mergeMap( (event: any) => this.users$, (event, users) => ({event, users: users.filter(user => user.group === "customer")}))
      .map( ({event, users}) => event ? this.filterUsers(event.target.value, users) : users );
  }

}

@Component({
  selector: 'pt-remove-user-dialog',
  styles: [
    `
    h2, p {
      font-family: 'Roboto', sans-serif;
    }
    `
  ],
  template: `
  <h2 md-dialog-title>Remove User</h2>
  <md-dialog-content>
    <p> Are you sure? </p>
  </md-dialog-content>
  <md-dialog-actions>
    <button md-button md-dialog-close>No</button>
    <button md-raised-button (click)="onRemove()" color="warn">Yes</button>
  </md-dialog-actions>
  `
})
export class PtRemoveCustomerDialog implements OnInit  {

  constructor(public dialogRef: MdDialogRef<PtRemoveCustomerDialog>, private pm: ProjectManagerService,
              public snackBar: MdSnackBar) {
  }

  ngOnInit() {
    console.log(this.dialogRef.config.data);
  }

  onRemove() {
    if(this.dialogRef.config.data.user) {
      this.snackBar.open(`Removing`, '', { duration: 2000 });
      this.pm.removeCustomer(this.dialogRef.config.data.user.$key).subscribe(user => {
        console.log('REMOVED CUSTOMER: ', user);
        this.snackBar.open(`Removed`, '', { duration: 2000 });
      });
      this.dialogRef.close();
    }
  }
}

@Component({
  selector: 'pt-user-edit-info',
  templateUrl: './pt-user-edit-info.component.html',
  styleUrls: ['./pt-user-edit-info.component.scss'],
  animations: [
    trigger('expandState', [
      state('out', style({height: 0})),
      state('in', style({height: "*"})),
      transition("in => out", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
      transition("out => in", animate("0.45s cubic-bezier(.4, 0, .2, 1)"))
    ]),
    trigger('collapserState', [
      state('expand', style({transform: 'rotateZ(180deg)'})),
      state('collapse', style({transform: 'rotateZ(0deg)'})),
      transition("expand <=> collapse", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
    ])
  ]
})
export class PtUserEditInfoDialog implements OnInit  {
  user_form: FormGroup;
  dealerships: any[];
  info: any = { selected: {}, expanded: {} };

  constructor(public dialogRef: MdDialogRef<PtUserEditInfoDialog>, private pm: ProjectManagerService,
              private fb: FormBuilder, public snackBar: MdSnackBar) {
  }

  filterDealerships(query: string) {
    if(!query) {
      return this.dealerships;
    }

    var filtered = [];
    for(let dealership of this.dealerships) {
      if(fuzzysearch(query, dealership.name))
        filtered.push(dealership);
    }
    return filtered;
  }

  toggleExpanded(key: string) {
    var e = this.info.expanded[key];
    if(e) {
      this.info.expanded[key] = false;
    } else {
      this.info.expanded[key] = true;
    }
  }

  ngOnInit() {
    console.log('Dialog config', this.dialogRef.config.data);
    let user_data = this.dialogRef.config.data.user;

    if(!user_data) {
      user_data = {
        name: '',
        email: '',
        phone: '',
        title: ''
      };
    }

    this.user_form = this.fb.group({
      name: [user_data.name || "", Validators.required],
      email: [user_data.email || "", Validators.compose([Validators.required, ptValidators.emailValidator]), ptValidators.userExistsValidatorFactory(this.pm, [user_data.email || 0])],
      phone: [user_data.phone || "", Validators.compose([Validators.required, ptValidators.phoneValidator])],
      title: [user_data.title || ""]
    });

    // Observable.from(this.pm.getAllDealerships())
    //   .subscribe((dealerships: any) => {
    //     console.log(dealerships);
    //     // if(!this.dealerships) {
    //       var tmp = [];
    //       var idx = 0;
    //       for(let d_uid of Object.keys(dealerships)) {
    //         var dealership = dealerships[d_uid];
    //         if(!dealership) { break; }
    //         console.log('Dealership', dealership.users);
    //         // var contains_user = false;
    //         // if(dealership.users) {
    //         //   for(let id of Object.keys(dealership.users)) {
    //         //     console.log("ID", id);
    //         //     if(user_data.$key === dealership.users[id].uid || user_data.$key === id) {
    //         //       this.info.selected[dealership.$key] = true;
    //         //       tmp.push(dealerships.splice(idx, 1)[0]);
    //         //       break;
    //         //     }
    //         //   }
    //         // }
    //         ++idx;
    //       }

    //       tmp = [...tmp, ...dealerships];
    //       this.dealerships = tmp;
    //     // }
    //     console.log('DEALERSHIPS: ', this.dealerships);
    //   })
  }

  onSave() {
    
    // var added_dealerships = Object.keys(this.info.selected).filter(k => this.info.selected[k] === true);
    // var removed_dealerships = Object.keys(this.info.selected).filter(k => this.info.selected[k] === false);
    // this.info.selected = {};
    // console.log('Add', added_dealerships, 'Remove', removed_dealerships);
    var user_uid = null;

    if(this.dialogRef.config.data.user) {
      user_uid = this.dialogRef.config.data.user.$key;
    }

    // Observable.from(added_dealerships)
    //   .mergeMap(dealership_uid => this.pm.addDealershipToUser(user_uid, dealership_uid), (dealership_uid, res) => dealership_uid)
    //   .mergeMap(dealership_uid => this.pm.addUserToDealership(false, user_uid, dealership_uid), (dealership_uid, res) => dealership_uid)
    //   .subscribe( res => {
    //     console.log('added to dealership', res);
    //   })

    // Observable.from(removed_dealerships)
    //   .mergeMap(dealership_uid => this.pm.removeCustomerFromDealership(user_uid, dealership_uid))
    //   .subscribe(res => {
    //     console.log("REMOVED USER FROM DEALERSHIP", res);
    //   });

    
    
    if(this.dialogRef.config.data.role === "edit" && user_uid) {
      this.snackBar.open(`Updating`, '', {
        duration: 2000
      });

      this.pm.updateUserInfo(user_uid, this.user_form.value).then(() => {
        this.snackBar.open(`Updated`, '', {
          duration: 2000
        });
      })

      // Observable.from(this.pm.updateUserInfo(user_uid, this.user_form.value))
      //   .mergeMap(r => Observable.from(added_dealerships)
      //         .mergeMap(dealership_uid => this.pm.addDealershipToUser(user_uid, dealership_uid), (dealership_uid, res) => dealership_uid)
      //         .mergeMap(dealership_uid => this.pm.addUserToDealership(false, user_uid, dealership_uid), (dealership_uid, res) => dealership_uid))
      //   .mergeMap(r => Observable.from(removed_dealerships)
      //         .mergeMap(dealership_uid => this.pm.removeCustomerFromDealership(user_uid, dealership_uid)))
      //   .subscribe(() => {
      //     this.snackBar.open(`Updated user`, '', {
      //       duration: 2000
      //     });
      //   }, (err) => {
      //     console.log('An error occured', err);
      //   });
    } else {
      this.snackBar.open(`Adding`, '', {
        duration: 2000
      });
      this.pm.createCustomer(this.user_form.value).subscribe(() => {
        this.snackBar.open(`Added user`, '', {
          duration: 2000
        });
      });
    }

    
  }
}