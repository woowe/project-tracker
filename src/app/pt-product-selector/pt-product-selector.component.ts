import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  trigger,
  state,
  style,
  transition,
  animate,
  ViewChild,
  AfterViewInit
} from '@angular/core';

import { MdDialog, MdDialogConfig } from '@angular/material';
import { MilestoneEditorComponent } from '../milestone-editor/milestone-editor.component';
import { ProjectManagerService } from '../services/ProjectManager/project-manager.service';
import { objectEq } from '../utils/utils';

@Component({
  selector: 'pt-product-selector',
  templateUrl: './pt-product-selector.component.html',
  styleUrls: ['./pt-product-selector.component.scss'],
  host: {
    '[class.pt-product-selector]': 'true'
  },
  animations: [
    trigger('expandState', [
      state('in', style({height: "*"})),
      state('out', style({height: 0})),
      transition("in <=> out", animate("0.45s cubic-bezier(.4, 0, .2, 1)"))
    ]),
    trigger('overflowState', [
      state('in', style({overflow: "visible"})),
      state('out', style({overflow: 'hidden'})),
      transition("in => out", animate("0.0s cubic-bezier(.4, 0, .2, 1)")),
      transition("out => in", animate("0.45s cubic-bezier(.4, 0, .2, 1)"))
    ]),
    trigger('collapserState', [
      state('in', style({transform: 'rotateZ(180deg)'})),
      state('out', style({transform: 'rotateZ(0deg)'})),
      transition("in <=> out", animate("0.45s cubic-bezier(.4, 0, .2, 1)")),
    ])
  ]
})
export class PtProductSelectorComponent implements OnInit {

  info: any = { selected: {}, expanded: {} };

  products_model: any[] = [];
  added_products_model: any[] = [];

  milestones_model: any;

  constructor(public milestone_dialog: MdDialog, private pm: ProjectManagerService) {}

  getKeys(obj: any) {
    return Object.keys(obj);
  }

  ngOnInit() {
  }

  @Input()
  set milestonesModel(milestones: any) {
    this.milestones_model = milestones;
  }

  @Input()
  get productsModel() {
    return this.products_model;
  }

  set productsModel(products: any) {
    this.products_model = products;

    console.log('SET PRODUCTS', products, this.products_model)
    for(let product of this.products_model) {
      // if(!product.selected)
      //   product.selected = false;
      
      if(!product.started)  {
        product.started = new Date(Date.now());
        product.activation = new Date(Date.now() + 8.64e+7 * 35);
      }
      else {
        product.started = new Date(Date.parse(product.started));
        product.activation = new Date(Date.parse(product.activation));
      }

      if(product.types) {
        for(let type of product.types) {
          type.default_milestone_template = type.default_milestone_template.toString();
        }
      }
    }

    
  }

  openMilestoneDialog(idx, template) {
    let dialogRef = this.milestone_dialog.open(MilestoneEditorComponent, { data: { template, milestone_model: this.products_model[idx].milestone_model } });
    dialogRef.afterClosed().subscribe( res => {
      console.log('AFTER CLOSED:', res);
      if(res) {
        this.products_model[idx].milestone_model = res;
      }
    });
  }

  toggleSelected(key: string) {
    let product = this.products_model.find(p => p.$key === key);

    if(product.op === 'UPDATE') {
      product.op = 'REMOVE';
    } else if(product.op === 'REMOVE') {
      product.op = 'UPDATE';
    }

    if(!product.op || product.op === 'NOOP') {
      product.op = "ADD";
    } else if(product.op === 'ADD') {
      product.op = "NOOP";
    }

    console.log("PRODUCT", product);

    if(product && product.selected) {
      if(!product.template && product.types && product.default_type) {
        product.template = product.types[product.default_type].default_milestone_template;
      }

      if(!product.product_type && product.types) {
        let product_type = product.types.find(t => t.default_milestone_template === product.template);
        product.product_type = product_type ? product_type.name : '';
      }

      if(!product.milestone_model && product.template) {
        this.pm.getMilestoneTemplate(product.template).subscribe(milestone => {
          product.milestone_model = milestone;
        });
      }
    }
  }

  toggleExpanded(key: string) {
    var e = this.info.expanded[key];
    if(e) {
      this.info.expanded[key] = e === 'in' ? 'out' : 'in';
    } else {
      this.info.expanded[key] = 'in';
    }
  }

  returnSelectedModel(key: string) {
    if(!this.info.selected[key]) {
      this.info.selected[key] = true;
    }
    return this.info.selected[key];
  }
}
